
package org.omarveliz.UserInterface;

import java.sql.ResultSet;
import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.omarveliz.connection.Conexion;
import org.omarveliz.handler.HandlerUsuario;
import org.omarveliz.bean.Usuario;
import org.omarveliz.login.Login;

public class Biblioteca extends Application {
    
    String image;
    private TabPane tabPane;
    private Tab tabPrincipal;
    private Tab tabPrestamos;
    private Tab tabUsuarios;
    private HBox hBox;
    private VBox vBoxPrincipal;
    
    private TextField textField;
    private TextField usuario;
    private PasswordField password;
    private VBox xBox;
    private ArrayList<Usuario> ArrayListUsuario; 
    private ObservableList<Usuario>  observableListUsuario1; 
    private TableColumn<Usuario, Integer> idUsuario;
    private Login login;
    
    @Override
    public void start(Stage primaryStage) {
        
        login = new Login();
        datosLibro datos = new datosLibro();
        
        observableListUsuario1 =
                FXCollections.observableArrayList(HandlerUsuario.getInstancia().getListaUsuario());
        
        MenuBar menuBarPrincipal = new MenuBar();
        menuBarPrincipal.getMenus().addAll(menuArchivo(), menuEdit());
        
        vBoxPrincipal = new VBox();
        
        hBox = new HBox();
        
        HBox hbox = new HBox ();
        GridPane Gridpane = new GridPane();
        Text titulo = new Text ("Tabla Usuario");
        titulo.setFont(Font.font("Tabla", FontWeight.NORMAL, 20));
        Gridpane.add(titulo, 0, 0);
        Button buscar = new Button("Buscar");
        Gridpane.add(buscar, 1, 1);
        
        TableColumn idUsuario = new TableColumn();
        idUsuario.setCellValueFactory(new PropertyValueFactory<Usuario,Integer>("idUsuario"));
        
        TableColumn idRol = new TableColumn();
        idRol.setCellValueFactory(new PropertyValueFactory<Usuario,Integer>("idRol"));
        
        TableColumn nombreUsuario = new TableColumn();
        nombreUsuario.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombreUsuario"));
        
        TableColumn contrasenia = new TableColumn ();
        contrasenia.setCellValueFactory(new PropertyValueFactory<Usuario,String>("Contrasenia"));
        
        TableView tabla = new TableView <> (observableListUsuario1);
        idUsuario.setText("ID Usuario");
        idRol.setText("ID Rol");
        nombreUsuario.setText("Nombre");
        contrasenia.setText("Contraseña");
        tabla.getColumns().addAll(idUsuario,idRol,nombreUsuario,contrasenia);
        Gridpane.add(tabla,0 ,2, 2, 1);
        Gridpane.setPadding(new Insets(20, 20, 20, 20));
        Gridpane.setHgap(75);
        Gridpane.setVgap(15);
        hbox.getChildren().addAll(Gridpane);
        
        tabPrincipal = new Tab("Usuarios");
        tabPrestamos = new Tab("Prestamos");
        tabUsuarios = new Tab("Libros");
        
        
        tabPrincipal.setContent(hbox);
        tabUsuarios.setContent(datos.stageLibro());
        
        tabPane = new TabPane();
        tabPane.getTabs().addAll(tabPrincipal,tabPrestamos,tabUsuarios);
        
        hBox.getChildren().addAll(tabPane);
        vBoxPrincipal.getChildren().addAll(menuBarPrincipal,tabPane);
        
        Scene scene = new Scene(vBoxPrincipal, 300, 250);
        
        primaryStage.setTitle("Omar Jacobo Muñoz Veliz 2013173");
        primaryStage.setScene(scene);
        primaryStage.show();
        login.Ventana();
        System.out.println(HandlerUsuario.getInstancia().autentificar("admin","admin"));
        System.out.println(HandlerUsuario.getInstancia().getUsuario().getIdUsuario());
        System.out.println(HandlerUsuario.getInstancia().getUsuario().getIdRol());
        System.out.println(HandlerUsuario.getInstancia().getUsuario().getNombreUsuario());
        System.out.println(HandlerUsuario.getInstancia().getUsuario().getContrasenia());
    }

    private Menu menuArchivo() {

        MenuItem menuConectar = new MenuItem("Conectar");
        MenuItem menuDesconectar = new MenuItem("Desconectar");
      
        menuDesconectar.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
        
        Menu menuArchivo = new Menu("Archivo");
        menuArchivo.getItems().addAll(menuConectar,menuDesconectar);
        return menuArchivo;
        
    }
   
    private Menu menuEdit() {

        MenuItem menuItemEliminar = new MenuItem("Eliminar");
        MenuItem menuItemCostos = new MenuItem("Costos");
        MenuItem menuItemBuscar = new MenuItem("Buscar");
        MenuItem menuItemPrestamos = new MenuItem("Prestamos");
        
        Menu menuEdit = new Menu("Edit");
        menuEdit.getItems().addAll(menuItemEliminar,menuItemCostos,menuItemBuscar,menuItemPrestamos);
        return menuEdit;
    }
      
    public static void main(String[] args) {
        launch(args);
    }
}
