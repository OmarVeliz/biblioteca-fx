

package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.omarveliz.bean.Libro;
import org.omarveliz.handler.handlerLibro;


public class datosLibro {
    
    private ArrayList<Libro> ArrayListLibro;
    private ObservableList<Libro> observableListLibro1; 
    private HBox hbOx;
    
    public HBox stageLibro(){
    observableListLibro1 =
                FXCollections.observableArrayList(handlerLibro.getInstancia().getListaLibro());
   
        hbOx = new HBox ();
        GridPane Gridpane = new GridPane();
        Text titulo = new Text ("Tabla Libro");
        titulo.setFont(Font.font("Libro", FontWeight.NORMAL, 20));
        Gridpane.add(titulo, 0, 0);
        Button buscar = new Button("Buscar");
        Gridpane.add(buscar, 1, 1);
        
        TableColumn idLibros = new TableColumn();
        idLibros.setCellValueFactory(new PropertyValueFactory<Libro,Integer>("idLibros"));
        
        TableColumn nombreLibro = new TableColumn();
        nombreLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreLibro"));
        
        TableColumn autorLibro = new TableColumn();
        autorLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("autorLibro"));
        
        TableColumn costoLibro = new TableColumn ();
        costoLibro.setCellValueFactory(new PropertyValueFactory<Libro,Integer>("costoLibro"));
        
        TableColumn fechaLibro = new TableColumn ();
        fechaLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("fechaLibro"));
        
        TableColumn generoLibro = new TableColumn ();
        generoLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("generoLibro"));
        
        TableView tab = new TableView <> (observableListLibro1);
        idLibros.setText("idLibros");
        nombreLibro.setText("nombreLibro");
        autorLibro.setText("autorLibro");
        costoLibro.setText("costoLibro");
        fechaLibro.setText("fechaLibro");
        generoLibro.setText("generoLibro");
        
        tab.getColumns().addAll(idLibros,nombreLibro,autorLibro,costoLibro,fechaLibro,generoLibro);
        Gridpane.add(tab,0 ,2, 2, 1);
        Gridpane.setPadding(new Insets(20, 20, 20, 20));
        Gridpane.setHgap(75);
        Gridpane.setVgap(15);
        hbOx.getChildren().addAll(Gridpane);    
        return hbOx;
    }
}
    
