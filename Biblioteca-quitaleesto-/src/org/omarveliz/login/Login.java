
package org.omarveliz.login;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.omarveliz.handler.HandlerUsuario;

public class Login {
        
    String image;
    private VBox vBoxPrincipal;
    private TextField textField;
    private TextField usuario;
    private PasswordField password;
    
    public void Ventana(){
        
        vBoxPrincipal = new VBox(15);
        
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        
        Text titulo = new Text("Autentificacion");
        grid.add(titulo,0,0,2,1);
        
        Label usuario;
        usuario = new Label("Usuario");
        usuario.setFont(Font.font("Usuario", FontWeight.BLACK, 20));
        grid.add(usuario,0,1);
        
        textField = new TextField();
        grid.add(textField,1,1);
        
        Label clave;
        clave = new Label("Contrasenia");
        clave.setFont(Font.font("Usuario", FontWeight.BLACK, 20));
        grid.add(clave,0,2);
        
        password = new PasswordField();
        grid.add(password,1,2);
        
        Button login = new Button("Acceder");
        grid.add(login, 1, 4);
       
        login.setOnAction(new EventHandler<ActionEvent>() {

            
             @Override
             public void handle(ActionEvent t) {
                  String usur = textField.getText();
                  String pass = password.getText();
                  System.out.println(HandlerUsuario.getInstancia().autentificar(usur, pass));
             }
         });
         
         image = "descarga.jpg";
         vBoxPrincipal.setStyle("-fx-background-image: url('" + image +"');" +"-fx-background-posicion: center;");
        
         vBoxPrincipal.getChildren().addAll(grid);
         Stage stage = new Stage();
         Scene esc = new Scene(vBoxPrincipal,300,200);
         stage.setTitle("Login");
         stage.setScene(esc);
         stage.show();
    }
}
