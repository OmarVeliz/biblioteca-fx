package org.omarveliz.handler;

import org.omarveliz.bean.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.connection.Conexion;

public class HandlerUsuario {
    private static HandlerUsuario instancia;
    //private Object conexion;
    private Usuario usuario;
    private ArrayList<Usuario> listaUsuario = new ArrayList();
    private HandlerUsuario() {
    }
    
    public static HandlerUsuario getInstancia() {
        if(instancia == null) {
            instancia = new HandlerUsuario();
        }
        return instancia;
    }
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Usuario WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Usuario user = new Usuario();
                    try {
                        user.setIdUsuario(resultset.getInt("IdUsuario"));
                        user.setIdRol(resultset.getInt("idRol"));
                        user.setNombreUsuario(resultset.getString("nombreUsuario"));
                        user.setContrasenia(resultset.getString("contrasenia"));
                        this.usuario = user;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
    
    public ArrayList<Usuario> getListaUsuario(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Usuario;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Usuario user = new Usuario();
                    try {
                        user.setIdUsuario(resultset.getInt("IdUsuario"));
                        user.setIdRol(resultset.getInt("idRol"));
                        user.setNombreUsuario(resultset.getString("nombreUsuario"));
                        user.setContrasenia(resultset.getString("contrasenia"));
                        listaUsuario.add(user);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaUsuario;
    } 
}


