package org.omarveliz.bean;

public class Usuario {
    private int idUsuario = 0;
    private int idRol = 0;
    private String nombreUsuario = null;
    private String contrasenia = null;
    
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    
    public int getIdUsuario(){
        return idUsuario;
    }
    
    public int getIdRol() {
        return idRol;
    }
    
    public String getNombreUsuario() {
        return nombreUsuario;
    }
    
    public String getContrasenia() {
        return contrasenia;
    }
}

