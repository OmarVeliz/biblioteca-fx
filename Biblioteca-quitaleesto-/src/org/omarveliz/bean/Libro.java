
package org.omarveliz.bean;

public class Libro {
    private int idLibros = 0;
    private String nombreLibro = null;
    private String autorLibro = null;
    private int costoLibro = 0;
    private String fechaLibro = null;
    private String generoLibro = null;
    
    public void setIdLibros(int idLibros) {
        this.idLibros = idLibros;
    }
    
    public void setNombreLibro(String nombreLibro) {
        this.nombreLibro = nombreLibro;
    }
    
    public void setAutorLibro(String autorLibro) {
        this.autorLibro = autorLibro;
    }
    
    public void setCostoLibro(int costoLibro) {
        this.costoLibro = costoLibro;
    }   
    
    public void setFechaLibro(String fechaLibro) {
        this.fechaLibro = fechaLibro;
    }
    
    public void setGeneroLibro(String generoLibro) {
        this.generoLibro = generoLibro;
    }
    
    public int getIdLibros() {
        return idLibros;
    }
    
    public String getNombreLibros() {
        return nombreLibro;
    }
    
    public String getAutorLibro() {
        return autorLibro;
    }
    
    public int getCostoLibro() {
        return costoLibro;
    }
    
    public String getFechaLibro() {
        return fechaLibro;
    }
    
    public String getGeneroLibro() {
        return generoLibro;
    }
    
}
