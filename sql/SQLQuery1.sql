CREATE DATABASE "ALIENWARE";
GO

USE ALIENWARE;
GO

CREATE TABLE Equipo(
	idEquipo INT NOT NULL,
	nombreProducto VARCHAR(50) NOT NULL,
	costoProducto INT NOT NULL,
	PRIMARY KEY(idEquipo)
);


CREATE TABLE Mantenimiento(
	idMantenimiento INT NOT NULL,
	idNumeroSerie INT  NOT NULL,
	idEquipo INT NOT NULL,
 	nombreUsuario VARCHAR(50) NOT NULL,
	costoMantenimiento INT NOT NULL,
	PRIMARY KEY(idMantenimiento),	
		FOREIGN KEY (idEquipo)
			REFERENCES Equipo(idEquipo)
);

CREATE TABLE Piezas(
	idPiezas INT NOT NULL,
	idMantenimiento INT NOT NULL,
	nombrePiezas VARCHAR(255) NOT NULL,
	marcaPiezas VARCHAR(255) NOT NULL,
	PRIMARY KEY (idPiezas),
		FOREIGN KEY (idMantenimiento)
		REFERENCES Mantenimiento(idMantenimiento)
);

CREATE TABLE Servicios(
	idServicios INT NOT NULL,
	idReparacion INT NOT NULL,
	idMantenimiento INT NOT NULL,
	idCuenta INT NOT NULL,
	nombreComputadora VARCHAR(50) NOT NULL,
	costoComputadora INT NOT NULL
	PRIMARY KEY(idServicios),
		FOREIGN KEY (idMantenimiento)
	REFERENCES Mantenimiento(idMantenimiento)
);

CREATE TABLE Reparacion(
	idReparacion INT NOT NULL,
	idServicios INT NOT NULL,
	nombreProblema VARCHAR(255) NOT NULL,
	casoProblemas VARCHAR(255) NOT NULL,
	costoReparacion DECIMAL(7,2) NOT NULL,
	PRIMARY KEY (idReparacion),
		FOREIGN KEY (idServicios)
		REFERENCES Servicios(idServicios)
);

CREATE TABLE Bitacora (
	idBitacora INT IDENTITY (1,1) NOT NULL,
	evento VARCHAR(255) NOT NULL,
	usuario VARCHAR(255) NOT NULL,
	horaFecha DATETIME NOT NULL
	PRIMARY KEY (idBitacora)
);

GO 

CREATE VIEW Servicios_Totales AS
	SELECT Mantenimiento.idMantenimiento, Mantenimiento.idNumeroSerie, Mantenimiento.nombreUsuario, Mantenimiento.costoMantenimiento,
	COUNT(*) AS 'Mantenimiento' FROM Servicios
	INNER JOIN Mantenimiento ON Servicios.idMantenimiento =Mantenimiento.idMantenimiento
	GROUP BY Mantenimiento.idMantenimiento,Mantenimiento.idNumeroSerie, Mantenimiento.nombreUsuario, Mantenimiento.costoMantenimiento
GO

--EQUIPO

CREATE PROCEDURE agregar_Equipo
@variableidEquipo INT,
@variablenombreProducto VARCHAR(255),
@variablecostoProducto INT
AS
BEGIN
	INSERT INTO Equipo(idEquipo, nombreProducto, costoProducto)
	VALUES (@variableidEquipo, @variablenombreProducto, @variablecostoProducto);
END
GO

CREATE PROCEDURE ModificarEquipo 
@variablecostoProducto INT,
@variablenombreProducto VARCHAR(255),
@variableidEquipo INT
AS
BEGIN
	UPDATE Equipo SET costoProducto= @variablecostoProducto,
	nombreProducto= @variablenombreProducto
	WHERE idEquipo = @variableidEquipo;
END
GO

--Mantenimiento

CREATE PROCEDURE agregar_Mantenimiento
@variableidMantenimiento INT,
@variableidnumeroSerie INT,
@variableidEquipo INT,
@variablenombreUsuario VARCHAR(255),
@variablecostoMantenimiento INT
AS
BEGIN
	INSERT INTO Mantenimiento(idMantenimiento, idNumeroSerie, idEquipo, nombreUsuario, costoMantenimiento)
	VALUES (@variableidMantenimiento, @variableidnumeroSerie, @variableidEquipo,@variablenombreUsuario,@variablecostoMantenimiento);
END
GO

CREATE PROCEDURE modificar_Mantenimiento
@variablenombreUsuario VARCHAR(255),
@variableidnumeroSerie INT,
@variableidEquipo INT,
@variablecostoMantenimiento INT,
@variableidMantenimiento INT
AS
BEGIN
	UPDATE Mantenimiento SET nombreUsuario= @variablenombreUsuario,
	idNumeroSerie= @variableidnumeroSerie,
	idEquipo= @variableidEquipo,
	costoMantenimiento= @variablecostoMantenimiento
	WHERE idMantenimiento = @variableidMantenimiento;
END
GO

--Piezas

CREATE PROCEDURE agregar_Piezas
@variableidPiezas INT,
@variableidMantenimiento INT,
@variablenombrePiezas VARCHAR(255),
@variablemarcaPiezas VARCHAR(255)
AS
BEGIN
	INSERT INTO Piezas(idPiezas, idMantenimiento, nombrePiezas, marcaPiezas)
	VALUES (@variableidPiezas, @variableidMantenimiento, @variablenombrePiezas, @variablemarcaPiezas );
END
GO

CREATE PROCEDURE modificar_Piezas
@variableidMantenimiento INT,
@variableidPiezas INT,
@variablenombrePiezas VARCHAR(255),
@variablemarcaPiezas VARCHAR(255)
AS
BEGIN
	UPDATE Piezas SET nombrePiezas= @variablenombrePiezas,
	marcaPiezas= @variablemarcaPiezas,
	idMantenimiento= @variableidMantenimiento
	WHERE idPiezas = @variableidPiezas;
END
GO

--Servicios

CREATE PROCEDURE ingresar_Servicio
@variableidServicios INT,
@variableidReparacion INT,
@variableidMantenimiento INT,
@variableidCuentas VARCHAR(255),
@variablenombreComputadora VARCHAR(50),
@variablecostoComputadora INT
AS
BEGIN
	INSERT INTO Servicios(idServicios, idReparacion, idMantenimiento, idCuenta,nombreComputadora, costoComputadora)
	VALUES (@variableidServicios, @variableidReparacion, @variableidMantenimiento, @variableidCuentas, @variablenombreComputadora, @variablecostoComputadora );
END
GO


CREATE PROCEDURE modificar_Servicio
@variableidServicios INT,
@variableidMantenimiento INT,
@variableidCuentas VARCHAR(255),
@variablenombreComputadora VARCHAR(50),
@variablecostoComputadora INT,
@variableidReparacion INT
AS
BEGIN
	UPDATE Servicios SET idServicios= @variableidServicios, 
	idMantenimiento= @variableidMantenimiento,
	idCuenta = @variableidCuentas,
	nombreComputadora= @variablenombreComputadora,
	costoComputadora= @variablecostoComputadora
	WHERE idReparacion = @variableidReparacion ;
END
GO

--Reparacion

CREATE PROCEDURE ingresar_Reparacion
@variableidReparacion INT,
@variableidServicios INT,
@variablenombreProblema VARCHAR(255),
@variablecasoProblema VARCHAR(255),
@variablecostoReparacion DECIMAL(7,2)
AS
BEGIN
	INSERT INTO Reparacion(idReparacion, idServicios, nombreProblema, casoProblemas, costoReparacion)
	VALUES (@variableidReparacion, @variableidServicios, @variablenombreProblema, @variablecasoProblema, @variablecostoReparacion);
END
GO

CREATE PROCEDURE modificar_Reparacion
@variableidReparacion INT,
@variableidServicios INT,
@variablenombreProblema VARCHAR(255),
@variablecasoProblema VARCHAR(255),
@variablecostoReparacion DECIMAL(7,2)
AS
BEGIN
	UPDATE Reparacion SET idReparacion= @variableidReparacion, 
	idServicios = @variableidServicios,
	nombreProblema= @variablenombreProblema,
	casoProblemas= @variablecasoProblema ,
	costoReparacion= @variablecostoReparacion
	WHERE costoReparacion = @variablecostoReparacion;
END
GO

--Eliminar Equipo

CREATE PROCEDURE eliminar_Equipo
@variableidEquipo INT
AS
BEGIN
	DELETE FROM Equipo WHERE idEquipo= @variableidEquipo ; 
END
GO

-----------------------------------------------------------------TRIGGER--------------------------------------------------------------------------
--Tabla Equipo
GO
CREATE TRIGGER agregar_otroEquipo
ON Equipo
AFTER INSERT 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha agregado equipo', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER modificar_nuevoEquipo
ON Equipo
AFTER UPDATE
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha modificado un equipo', CURRENT_USER, CURRENT_TIMESTAMP);
GO


CREATE TRIGGER eliminar_nuevoEquipo
ON Equipo
AFTER DELETE
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha eliminado un equipo', CURRENT_USER, CURRENT_TIMESTAMP);
GO

--Mantenimiento


CREATE TRIGGER ingresar_nuevoMantenimiento
ON Mantenimiento
AFTER INSERT
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha ingresado un mantenimiento', CURRENT_USER, CURRENT_TIMESTAMP);
GO


CREATE TRIGGER modificar_nuevoMantenimiento
ON Mantenimiento
AFTER UPDATE
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha modificado un mantenimiento', CURRENT_USER, CURRENT_TIMESTAMP);
GO


CREATE TRIGGER eliminar_nuevoMantenimiento
ON Equipo
AFTER DELETE
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha eliminado un mantenimiento', CURRENT_USER, CURRENT_TIMESTAMP);
GO

--Piezas

CREATE TRIGGER agregar_nuevoPiezas
ON Piezas
AFTER INSERT 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha agregado una pieza', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER modificar_nuevoPiezas
ON Piezas
AFTER UPDATE
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha modificado una pieza', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER eliminar_nuevoPiezas
ON Piezas
AFTER DELETE 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha eliminado una pieza', CURRENT_USER, CURRENT_TIMESTAMP);
GO

--Servicios

CREATE TRIGGER agregar_nuevoServicio
ON Servicios
AFTER INSERT 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha agregado un servicio', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER modificado_nuevoServicio
ON Servicios
AFTER UPDATE 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha agregado un servicio', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER eliminar_nuevoServicio
ON Servicios
AFTER DELETE 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha agregado un ervicio', CURRENT_USER, CURRENT_TIMESTAMP);
GO

--Reparacion

CREATE TRIGGER agregar_nuevoReparacion
ON Reparacion
AFTER INSERT 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha agregado una reparacion', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER modificar_nuevoReparacion
ON Reparacion
AFTER UPDATE 
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha modificado una reparacion', CURRENT_USER, CURRENT_TIMESTAMP);
GO

CREATE TRIGGER eliminar_nuevoReparacion
ON Reparacion
AFTER DELETE
AS
	INSERT INTO Bitacora(evento, usuario, horaFecha)
	VALUES ('Se ha eliminado una reparacion', CURRENT_USER, CURRENT_TIMESTAMP);
GO