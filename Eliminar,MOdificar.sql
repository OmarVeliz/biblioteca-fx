CREATE PROCEDURE modificarUsuario
@idUsuario INT,
@idRol INT,
@nombreUsuario VARCHAR(255),
@contrasenia VARCHAR(255)
AS
BEGIN 
	UPDATE Usuario SET idRol = @idRol, nombreUsuario = @nombreUsuario, contrasenia = CONVERT(VARCHAR(255),HASHBYTES ('MD5', @contrasenia),2)
	WHERE idUsuario = @idUsuario;
END

DELETE Usuario;

select * from Usuario;

INSERT INTO Usuario(idRol, nombreUsuario, contrasenia)
	VALUES	(1,'admin','admin');

EXEC modificarUsuario 11,1,'modif','modif';

GO
CREATE PROCEDURE buscar
@idUsuario INT
AS
BEGIN
	SELECT * FROM Usuario WHERE idUsuario = @idUsuario;
END

EXECUTE buscar 11;

GO
CREATE PROCEDURE eliminar
@idUsuario INT
AS
BEGIN 
	DELETE FROM Usuario WHERE idUsuario = @idUsuario;	
END

EXEC eliminar 11;

SELECT * FROM Usuario;
SELECT * FROM Libros;