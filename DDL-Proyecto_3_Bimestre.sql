CREATE TABLE Rol(
	idRol INT NOT NULL,
	rol VARCHAR(255) NOT NULL,
	PRIMARY KEY(idRol)
);

CREATE TABLE Usuario(
	idUsuario INT IDENTITY(1,1) NOT NULL,
	idRol INT NOT NULL,
	nombreUsuario VARCHAR(255) NOT NULL,
	contrasenia VARCHAR(255) NOT NULL,
	PRIMARY KEY(idUsuario),
		FOREIGN KEY(idRol) REFERENCES Rol(idRol)
);


CREATE TABLE Editorial(
	idEditorial INT IDENTITY(1,1) NOT NULL,
	nombreEditorial VARCHAR(255) NOT NULL,
	distribuidores VARCHAR(255) NOT NULL,
	PRIMARY KEY (idEditorial)
);

CREATE TABLE Genero(
	idGenero INT IDENTITY(1,1) NOT NULL,
	nombreGenero VARCHAR(255) NOT NULL,
	PRIMARY KEY(idGenero)
);

CREATE TABLE Autores(
	idAutor INT IDENTITY(1,1) NOT NULL,
	idEditorial INT NOT NULL,
	idGeneroAutor INT NOT NULL,
	nombreAutor VARCHAR(255) NOT NULL,
	ColeccionAutor VARCHAR(255) NOT NULL,
	PRIMARY KEY (idAutor),
		FOREIGN KEY (idEditorial) REFERENCES Editorial(idEditorial),
			FOREIGN KEY(idGeneroAutor) REFERENCES Genero(idGenero)
);

CREATE TABLE Libros(
	idLibros INT IDENTITY(1,1) NOT NULL,
	idAutorLibro INT NOT NULL,
	idGenero INT NOT NULL,
	idEditorial INT NOT NULL,
	nombreLibro VARCHAR(255) NOT NULL,
	estadoLibro VARCHAR(255) NOT NULL
	PRIMARY KEY(idLibros),
		FOREIGN KEY (idAutorLibro) REFERENCES Autores(idAutor)
);

CREATE TABLE Categoria(
	idCategoria INT IDENTITY(1,1),
	idLibro INT NOT NULL,
	nombreCategoria VARCHAR(255) NOT NULL,
	nombreGenero1 VARCHAR(255) NOT NULL,
	autoresCategoria VARCHAR(255) NOT NULL,
	PRIMARY KEY (idCategoria),	
		FOREIGN KEY (idLibro) REFERENCES Libros(idLibros)
);

CREATE TABLE Prestamos(
	idPrestamo INT IDENTITY(1,1) NOT NULL,
	idUsuario INT NOT NULL,
	idLibro INT NOT NULL,
	fechaPrestamo DATE NOT NULL,
	fechaDevolucion DATE NULL,
	PRIMARY KEY(idPrestamo),
		FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
		FOREIGN KEY (idLibro) REFERENCES Libros(idLibros)
);
--Rol

GO
CREATE VIEW vistaRol
AS
SELECT Usuario.idUsuario, Rol.rol, Usuario.nombreUsuario, Usuario.contrasenia FROM Usuario
	INNER JOIN Rol ON Usuario.idRol = Rol.idRol
GO	

SELECT * FROM vistaRol;

INSERT INTO Rol(idRol,rol) VALUES (1,'Administrador');
INSERT INTO Rol(idRol,rol) VALUES (2,'Editor');
INSERT INTO Rol(idRol,rol) VALUES (3,'Visualizador');
INSERT INTO Rol(idRol,rol) VALUES (4,'Alumno');

SELECT * FROM Rol;

SELECT * FROM vistaRol	
---Ajfksodf


GO
CREATE VIEW SeleccionLibros
AS
SELECT Libros.idLibros , nombreAutor, Genero.nombreGenero , Editorial.nombreEditorial , Libros.nombreLibro , Libros.estadoLibro FROM Libros
	INNER JOIN Autores ON Autores.idAutor = Libros.idAutorLibro 
	INNER JOIN Genero ON Libros.idGenero = Genero.idGenero
	INNER JOIN Editorial ON Libros.idEditorial = Editorial.idEditorial
GO

SELECT * FROM SeleccionLibros;

INSERT INTO Libros(idAutorLibro,idGenero,idEditorial,nombreLibro, estadoLibro)	VALUES (1,1,1,'El manantial','Ocupado');
SELECT * FROM Libros;
SELECT * FROM Autores;
SELECT * FROM Editorial;
SELECT * FROM Genero;
SELECT * FROM SeleccionLibros; 
--Procesos almacenados Usuario
GO
CREATE PROCEDURE modificarUsuario
@idUsuario INT,
@idRol INT,
@nombreUsuario VARCHAR(255),
@contrasenia VARCHAR(255)
AS
BEGIN 
	UPDATE Usuario SET idRol = @idRol, nombreUsuario = @nombreUsuario, contrasenia = CONVERT(VARCHAR(255),HASHBYTES ('MD5', @contrasenia),2)
	WHERE idUsuario = @idUsuario;
END
GO


GO
CREATE PROCEDURE eliminar
@idUsuario INT
AS
BEGIN 
	DELETE FROM Usuario WHERE idUsuario = @idUsuario;	
END
GO

GO
CREATE PROCEDURE autenticar
@usuario VARCHAR(200), @contrasenia VARCHAR(200)
AS
BEGIN
	SELECT * FROM Usuario WHERE nombreUsuario = @usuario AND contrasenia = CONVERT(VARCHAR(200),HASHBYTES('MD5', @contrasenia),2);
END
GO

GO
CREATE PROCEDURE insertarUsuario
@idRol INT, @nombreUsuario VARCHAR(255), @contrasenia VARCHAR(255)
AS
BEGIN
	INSERT INTO Usuario(idRol, nombreUsuario, contrasenia)
	VALUES (@idRol, @nombreUsuario, CONVERT(VARCHAR(255),HASHBYTES ('MD5', @contrasenia),2))
END
GO
EXEC insertarUsuario 1,'omar','omar';
GO
CREATE PROCEDURE BUSCAR
@CRITERIO VARCHAR(255)
AS
BEGIN
DECLARE @VARIABLE AS VARCHAR(255);
SET @VARIABLE = '%' + @CRITERIO + '%';
SELECT * FROM vistaRol WHERE 
idUsuario LIKE (@VARIABLE) OR rol LIKE (@VARIABLE) OR
nombreUsuario LIKE (@VARIABLE);
end
go 

EXECUTE BUSCAR 1;

---Libro

GO
CREATE PROCEDURE agregarL
@idAutorLibro INT,
@idGenero INT,
@idEditorial INT,
@nombreLibro VARCHAR(255),
@estadoLibro VARCHAR(255)
AS
BEGIN
	INSERT INTO Libros(idAutorLibro,idGenero,idEditorial,nombreLibro, estadoLibro) 
	VALUES (@idAutorLibro, @idGenero, @idEditorial, @nombreLibro, @estadoLibro);
END
GO
SELECT * FROM Libros;
EXEC agregarL 1,1,1,'Alquimista','Libre';

SELECT * FROM Usuario;
SELECT * FROM Libros;
SELECT * FROM Autores;
SELECT * FROM Editorial;
SELECT * FROM Categoria;
SELECT * FROM Genero;
SELECT * FROM Prestamos;

--Editorial

GO
CREATE PROCEDURE agregarEdi
@nombreEditorial VARCHAR(255),
@distribuidores VARCHAR(255)
AS
BEGIN
	INSERT INTO Editorial(nombreEditorial,distribuidores)
		VALUES (@nombreEditorial,@distribuidores);
END
GO

SELECT * FROM Editorial;

DELETE Editorial;

--Genero

GO
CREATE PROCEDURE agregarGenero
@nombreGenero VARCHAR(255)
AS 
BEGIN
	INSERT INTO Genero(nombreGenero) 
		VALUES (@nombreGenero);
END
GO

--Autores

GO
CREATE PROCEDURE agregarAut
@idEditorial INT,
@idGeneroAutor INT,
@nombreAutor VARCHAR(255),
@ColeccionAutor VARCHAR(255)
AS
BEGIN
	INSERT INTO Autores(idEditorial,idGeneroAutor,nombreAutor,ColeccionAutor)
		VALUES (@idEditorial,@idGeneroAutor,@nombreAutor,@ColeccionAutor);
END
GO

--Categoria

GO
CREATE PROCEDURE insertarCategoria
@idLibro INT,
@nombreCategoria VARCHAR(255),
@nombreGenero1 VARCHAR(255),
@autoresCategoria VARCHAR(255)
AS
BEGIN
	INSERT INTO Categoria(idLibro, nombreCategoria, nombreGenero1, autoresCategoria)
		VALUES (@idLibro, @nombreCategoria, @nombreGenero1, @autoresCategoria);
END
GO

--Prestamo

GO
CREATE PROCEDURE agregarPrestamo
@idUsuario INT,
@idLibro INT,
@fechaPrestamo VARCHAR(255),
@fechaDevolucion VARCHAR(255)
AS
BEGIN
	Insert into Prestamos(idUsuario,idLibro,fechaPrestamo,fechaDevolucion)
		VALUES(@idUsuario, @idLibro, @fechaPrestamo, @fechaDevolucion);
END
GO
SELECT * FROM Libros;
SELECT * FROM Usuario;
SELECT * FROM Prestamos;
EXEC agregarPrestamo 6,2,'2014/5/4','2014/4/12';

GO
CREATE PROCEDURE modificarGenero
@idGenero INT,
@nombreGenero VARCHAR(255)
AS
BEGIN 
	UPDATE Genero SET nombreGenero = @nombreGenero
	WHERE idGenero = @idGenero;
END
GO
EXEC modificarGenero 2,'Modif';
SELECT * FROM Genero;

---------------Genero
GO
CREATE PROCEDURE eliminarGenero
@idGenero INT
AS
BEGIN 
	DELETE FROM Genero WHERE idGenero = @idGenero;	
END
GO
-----------------Editorial
GO
CREATE PROCEDURE modificarE
@idEditorial INT,
@nombreEditorial VARCHAR(255),
@distribuidores VARCHAR(255)
AS
BEGIN
	UPDATE Editorial SET nombreEditorial = @nombreEditorial,
	distribuidores = @distribuidores
	WHERE idEditorial = @idEditorial;
END
GO
EXEC modificarE 1,'Modif','zona 1';
select * from Editorial;


GO
CREATE PROCEDURE eliminarEd
@idEditorial INT
AS
BEGIN
	DELETE FROM Editorial WHERE idEditorial = @idEditorial;
END
GO

EXEC eliminarEd 2;
-----------------------

GO
CREATE PROCEDURE modificarAut
@idAutor INT,
@idEditorial INT,
@idGenero INT,
@nombreAutor VARCHAR(255),
@coleccionAutor VARCHAR(255)
AS
BEGIN
	UPDATE Autores SET idEditorial = @idEditorial,
	idGeneroAutor = @idGenero,
	nombreAutor = @nombreAutor,
	ColeccionAutor = @coleccionAutor 
	WHERE idAutor = @idAutor;
END
GO

SELECT * FROM Autores;

GO
CREATE PROCEDURE eliminarAutores 
@idAutores INT
AS
BEGIN
	DELETE Autores WHERE idAutor = @idAutores;
END
GO

-----------------------------------------------lIBRO------------------------------------------------------
GO
CREATE PROCEDURE ModificarLibro
@idLibro INT,
@idAutor1 INT,
@idGenero INT,
@idEditorial INT,
@nombreLibro VARCHAR(255),
@estadoLibro VARCHAR(255)
AS
BEGIN
	UPDATE Libros SET idAutorLibro = @idAutor1,
	idGenero = @idGenero,
	idEditorial = @idEditorial,
	nombreLibro = @nombreLibro,
	estadoLibro = @estadoLibro
	WHERE idLibros = @idLibro; 
END
GO

GO
CREATE PROCEDURE EliminarLibr
@idLibro INT
AS
BEGIN
	DELETE Libros WHERE idLibros = @idLibro;
END
GO
SELECT * FROM Prestamos;

------------------------------------Prestamo------------------------------------------------

GO
CREATE PROCEDURE modificarPrest
@idPrestamo INT,
@idUsuario INT,
@idLibro INT,
@fechaPrestamo VARCHAR(255),
@fechaDevolucion VARCHAR(255)
AS
BEGIN
	UPDATE Prestamos SET idUsuario = @idUsuario,
	idLibro = @idLibro,
	fechaPrestamo = @fechaPrestamo,
	fechaDevolucion = @fechaDevolucion
	WHERE idPrestamo = @idPrestamo;
END
GO
EXEC modificarPrest 2,1,2,'2014-12-12','2014-12-12';

GO
CREATE PROCEDURE eliminarPrest
@idPrestamo INT
AS
BEGIN
	DELETE Prestamos WHERE idPrestamo = @idPrestamo;
END
GO
