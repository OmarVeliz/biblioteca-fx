CREATE TABLE Usuario(
	idUsuario INT IDENTITY(1,1) NOT NULL,
	idRol INT NOT NULL,
	nombreUsuario VARCHAR(255) NOT NULL,
	contrasenia VARCHAR(255) NOT NULL,
	PRIMARY KEY(idUsuario)
);

INSERT INTO Usuario(idRol, nombreUsuario, contrasenia ) VALUES 
	(1,'admin','admin');

CREATE TABLE Libros(
	idLibros INT IDENTITY(1,1) NOT NULL,
	nombreLibro VARCHAR(255) NOT NULL,
	autorLibro VARCHAR(255) NOT NULL,
	costoLibro INT NOT NULL,
	fechaLibro VARCHAR(255) NOT NULL,
	generoLibro VARCHAR(255) NOT NULL,
	PRIMARY KEY(idLibros)
)

INSERT INTO Libros (nombreLibro, autorLibro, costoLibro, fechaLibro , generoLibro)
	VALUES ('El manantial', 'Ayn Rand', 300, '3/2/14', 'Filosofia');

SELECT * FROM Usuario;
SELECT * FROM Libros;

GO
CREATE PROCEDURE insertarUsuario
@idRol INT, @nombreUsuario VARCHAR(255), @contrasenia VARCHAR(255)
AS
BEGIN
	INSERT INTO Usuario(idRol, nombreUsuario, contrasenia)
	VALUES (@idRol, @nombreUsuario, CONVERT(VARCHAR(255),HASHBYTES ('MD5', @contrasenia),2))
END
GO

EXECUTE insertarUsuario 3 ,'norman','norman';
EXECUTE insertarUsuario 4 ,'carlos','carlos';
EXECUTE insertarUsuario 3 ,'kevin','kevin';
SELECT * FROM Usuario;

--CREATE ALTER Modificar
