
package org.omarveliz.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.connection.Conexion;
import org.omarveliz.bean.Libro;
import org.omarveliz.connection.Conexion;

public class handlerLibro {
    
private static handlerLibro instancia;
    //private Object conexion;
    private Libro libro;
    private ArrayList<Libro> listaLibro = new ArrayList();
    
    private handlerLibro() {
    }
    
    public static handlerLibro getInstancia() {
        if(instancia == null) {
            instancia = new handlerLibro();
        }
        return instancia;
    }
    
    public Libro getLibro() {
        return libro;
    }
    
    public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Libros WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Libro libro = new Libro();
                    try {
                        libro.setIdLibros(resultset.getInt("idLibros"));
                        libro.setIdAutor(resultset.getInt("idAutorLibro"));
                        libro.setNombreLibro(resultset.getString("nombreLibro"));
                        libro.setAutorLibro(resultset.getString("autorLibro"));
                        libro.setCostoLibro(resultset.getInt("costoLibro"));
                        libro.setFechaLibro(resultset.getString("fechaLibro"));
                        libro.setGeneroLibro(resultset.getString("generoLibro"));
                        this.libro = libro;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
    
    public ArrayList<Libro> getListaLibro(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Libros;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Libro libro = new Libro();
                    try {
                        libro.setIdLibros(resultset.getInt("idLibros"));
                        libro.setIdAutor(resultset.getInt("idAutorLibro"));
                        libro.setNombreLibro(resultset.getString("nombreLibro"));
                        libro.setAutorLibro(resultset.getString("autorLibro"));
                        libro.setCostoLibro(resultset.getInt("costoLibro"));
                        libro.setFechaLibro(resultset.getString("fechaLibro"));
                        libro.setGeneroLibro(resultset.getString("generoLibro"));
                        listaLibro.add(libro);
                    } catch (SQLException ex) {
                        Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaLibro;
    }
    
    public void agregar(int idAutorLibro,String nombreLibro, String autorLibro, int costoLibro, String fechaLibro, String generoLibro) {
        Conexion.getInstancia().consultar("EXECUTE agregarL "+idAutorLibro+",'"+nombreLibro+"','"+autorLibro+"',"+costoLibro+",'"+fechaLibro+"','"+generoLibro+"';");
    }
    
}
