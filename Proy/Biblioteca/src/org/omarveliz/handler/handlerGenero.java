package org.omarveliz.handler;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import  org.omarveliz.bean.Genero;
import org.omarveliz.connection.Conexion;

public class handlerGenero {
    private static handlerGenero instancia;
    private Genero genero;
    private ArrayList<Genero> listaGenero= new ArrayList();
    private ArrayList<Genero> listaGeneroBuscar = new ArrayList();
    
     public static handlerGenero getInstancia() {
        if (instancia == null) {
            instancia = new handlerGenero();
        }
        return instancia;
    }

    public Genero getGenero() {
        return genero;
    }
    
    public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Editorial WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Genero genero1 = new Genero();
                    try {
                        genero1.setIdGenero(resultset.getInt("idGenero"));
                        genero1.setNombreGenero(resultset.getString("nombreGenero"));
                        this.genero = genero1;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
     
     public ArrayList<Genero> getListaGenero(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Genero;;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Genero genero2 = new Genero();
                    try {
                        genero2.setIdGenero(resultset.getInt("idGenero"));
                        genero2.setNombreGenero(resultset.getString("nombreGenero"));         
                        listaGenero.add(genero2);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaGenero;
    } 
    
     
     public void agregarGenero(String nombreGenero) {
         Conexion.getInstancia().consultar("EXECUTE agregarGenero '"+nombreGenero+"';");     
     }
}
