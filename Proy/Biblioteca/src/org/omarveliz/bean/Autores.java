package org.omarveliz.bean;

public class Autores {
  private int idAutor = 0;
  private int idEditorial = 0;
  private int idGeneroAutor = 0;
  private String nombreAutor = null;
  private String ColeccionAutor = null;
  private String generoAutor = null;
  
  public void setIdAutor(int idAutor) {
      this.idAutor = idAutor;
  }
  
  public void setIdEditorial(int idEditorial) {
      this.idEditorial = idEditorial;
  }
  
  public void setIdGeneroAutor(int idGeneroAutor) {
      this.idGeneroAutor = idGeneroAutor;
  }
 
  public void setNombreAutor(String nombreAutor) {
      this.nombreAutor = nombreAutor;
  }
  
  public void setColeccionAutor(String ColeccionAutor) {
      this.ColeccionAutor = ColeccionAutor;
  }
  
  public void setGeneroAutor(String generoAutor) {
      this.generoAutor = generoAutor;
  }
  
  public int getIdAutor() {
      return idAutor;
  }
  
  public int getIdEditorial() {
      return idEditorial;
  }
  
  public int getIdGeneroAutor() {
      return idGeneroAutor;
  }
  
  public String getNombreAutor() {
      return nombreAutor;
  }
  
  public String getColeccionAutor() {
      return ColeccionAutor;
  }
  
  public String getGeneroAutor() {
      return generoAutor;
  }
}
 