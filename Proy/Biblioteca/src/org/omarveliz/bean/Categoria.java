package org.omarveliz.bean;

public class Categoria {
    
    private int idCategoria = 0;
    private int idLibro = 0;
    private String nombreCategoria = null;
    private String nombreGenero1 = null;
    private String autoresCategoria = null;
    
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }
            
    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }
    
    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }
    
    public void setNombreGenero(String nombreGenero1) {
        this.nombreGenero1 = nombreGenero1;
    }
    
    public void setAutoresCategoria(String autoresCategoria) {
        this.autoresCategoria = autoresCategoria;
    }
    
    public int getIdCategoria() {
        return idCategoria;
    }

    public int getIdLibro() {
        return idLibro;
    }
    
    public String getNombreCategoria() {
        return nombreCategoria;
    }
    
    public String getNombreGenero1() {
        return nombreGenero1;
    }
    
    public String getAutoresCategoria() {
        return autoresCategoria;
    }
}
