package org.omarveliz.bean;

public class Genero {
    private int idGenero = 0;
    private String nombreGenero = null;
    
    public Genero() {
    
    }
    
    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }
    
    public void setNombreGenero(String nombreGenero) {
        this.nombreGenero = nombreGenero;
    }
    
    public int getIdGennero() {
        return idGenero;
    }
    
    public String nombreGenero() {
        return nombreGenero;
    }
}
