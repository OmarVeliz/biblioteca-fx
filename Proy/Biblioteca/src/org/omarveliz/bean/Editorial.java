
package org.omarveliz.bean;

public class Editorial {
    
    private int idEditorial = 0;
    private String nombreEditorial = null;
    private String distribuidores = null;
    
    public void setIdEditorial(int idEditorial) {
        this.idEditorial = idEditorial;
    }
    
    public void setNombreEditorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }
    
    public void setDistribuidores(String distribuidores) {
        this.distribuidores = distribuidores;
    }
    
    public int getIdEditorial() {
        return idEditorial;
    }
    
    public String getNombreEditorial() {
        return nombreEditorial;
    }
    
    public String getDistribuidores() {
        return distribuidores;
    }
} 
