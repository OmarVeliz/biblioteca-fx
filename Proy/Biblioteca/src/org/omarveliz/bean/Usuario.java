package org.omarveliz.bean;

public class Usuario {
    private int idUsuario = 0;
    private String idRol = null;
    private String nombreUsuario = null;
    private String contrasenia = null;
    
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    
    public int getIdUsuario(){
        return idUsuario;
    }
    
    public String getIdRol() {
        return idRol;
    }
    
    public String getNombreUsuario() {
        return nombreUsuario;
    }
    
    public String getContrasenia() {
        return contrasenia;
    }
}

