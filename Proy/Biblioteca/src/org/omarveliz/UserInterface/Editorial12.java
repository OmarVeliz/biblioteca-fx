
package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.omarveliz.handler.handlerEditorial;
import org.omarveliz.bean.Editorial;

public class Editorial12 {
    
    private ArrayList<Editorial> ArrayListEdit;
    private ObservableList<Editorial> observableListEdit1; 
    private HBox hbOx2;
    private HBox hbox3;
    private GridPane agregarGrid;
    private TextField idEditorial2;
    private TextField nombreEdit;
    private TextField distribuidores2;
    private TableView tab1;
    
     public HBox stageEditorial(){
      observableListEdit1 =
              FXCollections.observableArrayList(handlerEditorial.getInstancia().getListaEditorial());
      
      hbOx2 = new HBox();
      GridPane gridEdit = new GridPane();
      Text titulo = new Text ("Tabla Editorial");
      titulo.setFont(Font.font("Editoriales", FontWeight.NORMAL, 20));
      gridEdit.add(titulo, 0, 0);
      Button buscar = new Button("Buscar");
      gridEdit.add(buscar, 1, 1);
      
      TableColumn idEditorial = new TableColumn();
      idEditorial.setCellValueFactory(new PropertyValueFactory<Editorial,Integer>("idEditorial"));
        
      TableColumn nombreEditorial = new TableColumn();
      nombreEditorial.setCellValueFactory(new PropertyValueFactory<Editorial,String>("nombreEditorial"));
        
      TableColumn distribuidores = new TableColumn();
      distribuidores.setCellValueFactory(new PropertyValueFactory<Editorial,String>("distribuidores"));
         
      tab1 = new TableView <> (observableListEdit1);
      idEditorial.setText("IdEditorial");
      nombreEditorial.setText("nombreEditorial");
      distribuidores.setText("distribuidores");
         
      tab1.getColumns().addAll(idEditorial,nombreEditorial,distribuidores);
      
      //agregarEditorial
      
      hbox3 = new HBox();
      GridPane agregarEdit1 = new GridPane();
      agregarEdit1.setVgap(5);
      agregarEdit1.setHgap(5);
      
       Label RegistroL;
        RegistroL = new Label("Ingresar Editorial");
        RegistroL.setFont(Font.font("Ingresar Editorial", FontWeight.BLACK, 20));
        agregarEdit1.add(RegistroL,0,1);
        
        Label idEditorial1;
        idEditorial1 = new Label("idEditorial");
        agregarEdit1.add(idEditorial1,0,2);
          
        idEditorial2 = new TextField();
        agregarEdit1.add(idEditorial2,1,2);
        
        Label nombreEditorial1;
        nombreEditorial1 = new Label("Nombre Editorial");
        agregarEdit1.add(nombreEditorial1,0,3);
         
        nombreEdit = new TextField();
        agregarEdit1.add(nombreEdit,1,3);
        
        Label distribuidores1;
        distribuidores1 = new Label("Distribuidores");
        agregarEdit1.add(distribuidores1,0,4);
       
        distribuidores2 = new TextField();
        agregarEdit1.add(distribuidores2,1,4);
        
        Button agregarEdit12 = new Button("Agregar");
        agregarEdit1.add(agregarEdit12,1,5);
        
        agregarEdit12.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
              handlerEditorial.getInstancia().agregarEdit(nombreEdit.getText() ,distribuidores2.getText());
            }
        });
        
        
        hbox3.getChildren().addAll(agregarEdit1);
        gridEdit.add(tab1,0,2,2,1);
        gridEdit.setPadding(new Insets(20, 20, 20, 20));
        gridEdit.setVgap(15);
        gridEdit.setHgap(75);
        hbOx2.getChildren().addAll(gridEdit,hbox3);
        return hbOx2;
    }
     
     public void limpiarL(){
        observableListEdit1.clear();
        handlerEditorial.getInstancia().getListaEditorial().clear();
        observableListEdit1 =  FXCollections.observableArrayList(handlerEditorial.getInstancia().getListaEditorial());
        
        tab1.setItems(observableListEdit1);
    }
     
}
