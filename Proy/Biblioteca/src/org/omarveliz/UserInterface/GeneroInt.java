package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.omarveliz.bean.Genero;
import org.omarveliz.handler.handlerGenero;
        
public class GeneroInt {
    
    private ArrayList<Genero> ArrayListEdit;
    private ObservableList<Genero> observableListGener1; 
    private HBox generBox;
    private HBox generBox2;
    private GridPane generoGrid;
    private TextField idGenero14;
    private TextField nombreGenero1;
    private TableView tabG;
    
    
    public HBox stageGenero(){
      observableListGener1 =
              FXCollections.observableArrayList(handlerGenero.getInstancia().getListaGenero());
      
      generBox = new HBox();
      GridPane gridGener = new GridPane();
      Text titulo = new Text ("Tabla Genero");
      titulo.setFont(Font.font("Generos", FontWeight.NORMAL, 20));
      gridGener.add(titulo, 0, 0);
      Button buscar = new Button("Buscar");
      gridGener.add(buscar, 1, 1);
      
      TableColumn idGenero = new TableColumn();
      idGenero.setCellValueFactory(new PropertyValueFactory<Genero,Integer>("idGenero"));
        
      TableColumn nombreGenero = new TableColumn();
      nombreGenero.setCellValueFactory(new PropertyValueFactory<Genero,String>("nombreGenero"));   
         
      tabG = new TableView <> (observableListGener1);
      idGenero.setText("idGenero");
      nombreGenero.setText("nombreGenero"); 
      tabG.getColumns().addAll(idGenero,nombreGenero);
      
      //agregarEditorial
      
      generBox2 = new HBox();
      GridPane agregarGener2 = new GridPane();
      agregarGener2.setVgap(5);
      agregarGener2.setHgap(5);
      
       Label RegistroL;
        RegistroL = new Label("Ingresar Editorial");
        RegistroL.setFont(Font.font("Ingresar Editorial", FontWeight.BLACK, 20));
        agregarGener2.add(RegistroL,0,1);
        
        Label idGenero12;
        idGenero12 = new Label("idGenero(Modificar)");
        agregarGener2.add(idGenero12,0,2);
          
        idGenero14 = new TextField();
        agregarGener2.add(idGenero14,1,2);
        
        Label nombreGenero12;
        nombreGenero12 = new Label("Nombre Genero");
        agregarGener2.add(nombreGenero12,0,3);
         
        nombreGenero1 = new TextField();
        agregarGener2.add(nombreGenero1,1,3);
        
        
        Button agregarGenero = new Button("Agregar");
        agregarGener2.add(agregarGenero,1,5);
        
        agregarGenero.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
              handlerGenero.getInstancia().agregarGenero(nombreGenero1.getText());
              limpiarL();
            }
        });
        
        
        generBox2.getChildren().addAll(agregarGener2);
        gridGener.add(tabG,0,2,2,1);
        gridGener.setPadding(new Insets(20, 20, 20, 20));
        gridGener.setVgap(15);
        gridGener.setHgap(75);
        generBox.getChildren().addAll(gridGener,generBox2);
        return generBox;
    }
    
   public void limpiarL(){
        observableListGener1.clear();
        handlerGenero.getInstancia().getListaGenero().clear();
        observableListGener1 =  FXCollections.observableArrayList(handlerGenero.getInstancia().getListaGenero());
        
        tabG.setItems(observableListGener1);
    }
    
}
