package org.omarveliz.UserInterface;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.omarveliz.bean.Usuario;
import org.omarveliz.connection.Conexion;
import org.omarveliz.handler.HandlerUsuario;
import org.omarveliz.handler.handlerLibro;
import org.omarveliz.login.Login;
import org.omarveliz.UserInterface.Editorial12;
import org.omarveliz.handler.handlerEditorial;

public class Biblioteca extends Application {
    
    private TabPane tabPane;
    private Tab tabPrincipal;
    private Tab tabPrestamos;
    private Tab tabUsuarios;
    private Tab tabGenero;
    private Tab Autores1123;
    private Tab tabEditoriales;
    private HBox hBox;
    private VBox vBoxPrincipal;
    private HBox Hbox;
    private TextField rol; 
    private TextField nombre;
    private PasswordField password1;
    private Usuario usuario1;
    private TextField textField;
    private TextField usuario;
    private PasswordField password;
    private VBox xBox;
    private ArrayList<Usuario> ArrayListUsuario; 
    private ObservableList<Usuario>  observableListUsuario1; 
    private TableColumn<Usuario, Integer> idUsuario;
    private Login login;
    private TableView tabla;
    private TextField buscar12;
    private TextField modificar;
    private HBox modificar2;
    private GridPane grid1;
    private TextField idUsuario2;
    private TextField eliminar11;
    private Stage stage;
    
    @Override
    public void start(Stage primaryStage) {
        
        login = new Login();
        datosLibro datos = new datosLibro();
        Editorial12 edit = new Editorial12();
        GeneroInt gener1 = new GeneroInt();
        AutoresInt aut = new AutoresInt();
        
        observableListUsuario1 =
                FXCollections.observableArrayList(HandlerUsuario.getInstancia().getListaUsuario());
        
        MenuBar menuBarPrincipal = new MenuBar();
        menuBarPrincipal.getMenus().addAll(menuArchivo(), menuUsuarios(), menuRoles(), menuEditoriales(), menuAutores(), menuGeneros(), menuCategoria(), menuPrestamo(), menuReporte() );
        
        vBoxPrincipal = new VBox();
        
        hBox = new HBox();
        
        HBox hbox = new HBox ();
        GridPane Gridpane = new GridPane();
        Text titulo = new Text ("Tabla Usuario");
        titulo.setFont(Font.font("Tabla", FontWeight.NORMAL, 20));
        Gridpane.add(titulo, 0, 0);
        
        buscar12 = new TextField();
        Gridpane.add(buscar12,1,1);
        
        TableColumn idUsuario = new TableColumn();
        idUsuario.setCellValueFactory(new PropertyValueFactory<Usuario,Integer>("idUsuario"));
        
        TableColumn idRol = new TableColumn();
        idRol.setCellValueFactory(new PropertyValueFactory<Usuario,String>("idRol"));
        
        TableColumn nombreUsuario = new TableColumn();
        nombreUsuario.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombreUsuario"));
        
        TableColumn contrasenia = new TableColumn ();
        contrasenia.setCellValueFactory(new PropertyValueFactory<Usuario,String>("Contrasenia"));
        
        //Usuarios
        Hbox = new HBox();
        GridPane gridpane = new GridPane();
        gridpane.setHgap(5);
        gridpane.setVgap(5);
        
        Label Registro;
        Registro = new Label("Registro");
        Registro.setFont(Font.font("Registro", FontWeight.BLACK, 20));
        gridpane.add(Registro,0,1);
        
        Label idUsuario1;
        idUsuario1 = new Label("ID Usuario(Modificar)");
        gridpane.add(idUsuario1,0,2);
        
        idUsuario2 = new TextField();
        gridpane.add(idUsuario2,1,2);
        
        Label rolUsusario;
        rolUsusario = new Label("Rol");
        gridpane.add(rolUsusario,0,3);
        
        rol = new TextField();
        gridpane.add(rol,1,3);
        
        Label NombreUsuario;
        NombreUsuario = new Label("Nombre Usuario");
        gridpane.add(NombreUsuario, 0, 4);
        
        nombre = new TextField();
        gridpane.add(nombre,1,4);
        
        Label contraseniaUsuario;
        contraseniaUsuario = new Label("Contrasenia");
        gridpane.add(contraseniaUsuario,0,5);
                 
        password1 = new PasswordField();
        gridpane.add(password1,1,5);
        
        eliminar11 = new TextField();
        gridpane.add(eliminar11, 0, 7);
        
        Button crear = new Button("Crear");
        gridpane.add(crear, 1, 6);
        
        Button eliminar = new Button("Eliminar");
        gridpane.add(eliminar, 1, 7);
        
        Button Buscar1 = new Button("Buscar");
        Gridpane.add(Buscar1,0,1);
        
        Button modificarR = new Button("Modificar");
        gridpane.add(modificarR,1,8);
                
        Button eliminarUsuario = new Button("Vaciar tablas");
        gridpane.add(eliminarUsuario, 1, 10);
        
        Button refrescar = new Button("Refrescar tabla");
        Gridpane.add(refrescar,0,3);
        
        Button lenguaje = new Button("English");
        gridpane.add(lenguaje,0,10);
        
        lenguaje.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                loadView(new Locale("en", "EN"));
            }
        });
 
        crear.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                HandlerUsuario.getInstancia().agregar(rol.getText(), nombre.getText(), password1.getText());
                limpiar();
            }
        });
        
        eliminar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
               HandlerUsuario.getInstancia().eliminarLista(Integer.parseInt(eliminar11.getText()));
               limpiar();
               }
        });
        
        Buscar1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
               buscar3(buscar12.getText());  
            }
        });
        
        modificarR.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
               HandlerUsuario.getInstancia().modificarUsuario(Integer.parseInt(idUsuario2.getText()),rol.getText(),nombre.getText(), password1.getText());
               limpiar();
            }
        });
        
        eliminarUsuario.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                 HandlerUsuario.getInstancia().eliminar();
                 limpiar();
            }
        });
        
        refrescar.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                limpiar();
            }
        });
        
        Hbox.getChildren().addAll(gridpane);
        
        tabla = new TableView <> (observableListUsuario1);
        idUsuario.setText("ID Usuario");
        idRol.setText("ID Rol");
        nombreUsuario.setText("Nombre");
        contrasenia.setText("Contraseña");
        tabla.getColumns().addAll(idUsuario,idRol,nombreUsuario,contrasenia);
        Gridpane.add(tabla,0 ,2, 2, 1);
        Gridpane.setPadding(new Insets(20, 20, 20, 20));
        Gridpane.setHgap(75);
        Gridpane.setVgap(15);
        hbox.getChildren().addAll(Gridpane,Hbox);
        
        tabPrincipal = new Tab("Usuarios");
        tabPrestamos = new Tab("Prestamos");
        tabUsuarios = new Tab("Libros");
        tabEditoriales = new Tab("Editoriales");
        tabGenero = new Tab("Generos");
        Autores1123 = new Tab("Autores");
        
        tabEditoriales.setContent(edit.stageEditorial());
        Autores1123.setContent(aut.stageLibro());
        
        tabPrincipal.setContent(hbox);
        tabUsuarios.setContent(datos.stageLibro());
        tabGenero.setContent(gener1.stageGenero());
        tabPane = new TabPane();
        tabPane.getTabs().addAll(tabPrincipal,tabPrestamos,tabUsuarios,tabEditoriales,tabGenero,Autores1123);
        
        hBox.getChildren().addAll(tabPane);
        hBox.getChildren().add(new StackPane());
        
        vBoxPrincipal.getChildren().addAll(menuBarPrincipal,tabPane);
     
        Scene scene = new Scene(vBoxPrincipal, 800, 400);
        
        
        primaryStage.setTitle("Omar Jacobo Muñoz Veliz 2013173");
        primaryStage.setScene(scene);
        primaryStage.show();
        login.Ventana();
    }

    private Menu menuArchivo() {

        MenuItem menuConectar = new MenuItem("Conectado");
        MenuItem menuDesconectar = new MenuItem("Desconectar");
      
        menuDesconectar.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
        
        Menu menuArchivo = new Menu("Archivo");
        menuArchivo.getItems().addAll(menuConectar,menuDesconectar);
        return menuArchivo;
        
    }
   
    private Menu menuUsuarios() {

        MenuItem menuItemCrear = new MenuItem("Crear");
        MenuItem menuItemModificar = new MenuItem("Modificar");
        MenuItem menuItemBuscar = new MenuItem("Buscar");
        MenuItem menuItemEliminar = new MenuItem("Eliminar");
        
        Menu menuUsuarios = new Menu("Usuarios");
        menuUsuarios.getItems().addAll(menuItemCrear,menuItemModificar,menuItemBuscar,menuItemEliminar);
        return menuUsuarios;
    }
    
    private Menu menuRoles() {
        
        MenuItem menuItemcrear = new MenuItem("Crear");
        MenuItem menuItemModificar = new MenuItem("Modificar");
        MenuItem menuItemBuscar = new MenuItem("Buscar");
        MenuItem menuItemEliminar = new MenuItem("Eliminar");
        MenuItem menuItemEditar = new MenuItem("Editor");
        MenuItem menuItemReporte = new MenuItem("Reporte de alumnos");
        
        Menu menuRoles = new Menu("Roles");
        menuRoles.getItems().addAll(menuItemcrear,menuItemModificar, menuItemBuscar,menuItemEliminar, menuItemEditar,menuItemReporte  );
        return menuRoles;
    }
      
    private Menu menuEditoriales() {
        
        MenuItem menuItemCrea1 = new MenuItem("Crear");
        MenuItem menuItemModificar1 = new MenuItem("Modificar");
        MenuItem menuItemBuscar1 = new MenuItem("Buscar");
        MenuItem menuItemEliminar1 = new MenuItem("Eliminar");
        
        Menu menuEditoriales = new Menu("Editoriales");
        menuEditoriales.getItems().addAll(menuItemCrea1,menuItemModificar1, menuItemBuscar1, menuItemEliminar1 );
        return menuEditoriales;
    }
    
    private Menu menuAutores() {
    
        MenuItem menuItemAutores = new MenuItem("Crear");
        MenuItem menuItemModificar2 = new MenuItem("Modificar");
        MenuItem menuItemBuscar2 = new MenuItem("Buscar");
        MenuItem menuItemEliminar2 = new MenuItem("Eliminar");
        
        Menu menuAutores = new Menu("Autores");
        menuAutores.getItems().addAll(menuItemAutores, menuItemModificar2, menuItemBuscar2,menuItemEliminar2 );
        return menuAutores;
    }
    
    private Menu menuGeneros() {
    
        MenuItem menuItemGeneros = new MenuItem("Crear");
        MenuItem menuItemModificar3 = new MenuItem("Modificar");
        MenuItem menuItemBuscar3 = new MenuItem("Buscar");
        MenuItem menuItemEliminar3 = new MenuItem("Eliminar");
        
        Menu menuGeneros = new Menu("Generos");
        menuGeneros.getItems().addAll(menuItemGeneros,menuItemModificar3, menuItemBuscar3, menuItemEliminar3 );
        return menuGeneros;
    }
    
    private Menu menuCategoria() {
        
        MenuItem menuItem = new MenuItem("Categoria");
        
        Menu menuCategoria = new Menu("Categoria de libros");
        menuCategoria.getItems().addAll(menuItem);
        return menuCategoria;
    }
    
    private Menu menuPrestamo() {
        Menu menuPrestamo = new Menu("Prestamo");
        return menuPrestamo;
    }
    
    private Menu menuReporte() {
       
        Menu menuReporte = new Menu("Reporte");
        return menuReporte;
    }
    
    public void limpiar ()  {
        observableListUsuario1.clear();
        HandlerUsuario.getInstancia().getListaUsuario().clear();
        observableListUsuario1 =  FXCollections.observableArrayList(HandlerUsuario.getInstancia().getListaUsuario());
        
        tabla.setItems(observableListUsuario1);
    } 
    
    public void buscar3(String criterio){
       observableListUsuario1.clear();
       HandlerUsuario.getInstancia().getListaUsuario().clear(); 
       observableListUsuario1 = FXCollections.observableArrayList(HandlerUsuario.getInstancia().getListaBuscar(criterio));
       tabla.setItems(observableListUsuario1);
    }
    
    private void loadView(Locale locale) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setResources(ResourceBundle.getBundle("bundles.MyBundle", locale));
            Pane pane = (BorderPane) fxmlLoader.load(this.getClass().getResource("MyView.fxml").openStream());
            // replace the content
            StackPane content = (StackPane) ((VBox) stage.getScene().getRoot()).getChildren().get(1);
            content.getChildren().clear();
            content.getChildren().add(pane);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
