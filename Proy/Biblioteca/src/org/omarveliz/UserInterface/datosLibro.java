
package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.omarveliz.bean.Libro;
import org.omarveliz.handler.handlerLibro;

public class datosLibro {
    
    private ArrayList<Libro> ArrayListLibro;
    private ObservableList<Libro> observableListLibro1; 
    private HBox hbOx;
    private HBox hbox1;
    private GridPane agregarGrid;
    private TextField idAutor2;
    private TextField nombreL;
    private TextField autorL;
    private TextField CostoL;
    private TextField fechaL;
    private TextField generoL;
    private TableView tab;
    
    public HBox stageLibro(){
    observableListLibro1 =
                FXCollections.observableArrayList(handlerLibro.getInstancia().getListaLibro());
   
        hbOx = new HBox ();
        GridPane Gridpane = new GridPane();
        Text titulo = new Text ("Tabla Libro");
        titulo.setFont(Font.font("Libro", FontWeight.NORMAL, 20));
        Gridpane.add(titulo, 0, 0);
        Button buscar = new Button("Buscar");
        Gridpane.add(buscar, 1, 1);
        
        TableColumn idLibros = new TableColumn();
        idLibros.setCellValueFactory(new PropertyValueFactory<Libro,Integer>("idLibros"));
        
        TableColumn idAutorLibro = new TableColumn();
        idAutorLibro.setCellValueFactory(new PropertyValueFactory<Libro,Integer>("idAutorLibro"));
        
        TableColumn nombreLibro = new TableColumn();
        nombreLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreLibro"));
        
        TableColumn autorLibro = new TableColumn();
        autorLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("autorLibro"));
        
        TableColumn costoLibro = new TableColumn ();
        costoLibro.setCellValueFactory(new PropertyValueFactory<Libro,Integer>("costoLibro"));
        
        TableColumn fechaLibro = new TableColumn ();
        fechaLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("fechaLibro"));
        
        TableColumn generoLibro = new TableColumn ();
        generoLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("generoLibro"));
        
        tab = new TableView <> (observableListLibro1);
        idLibros.setText("idLibros");
        idAutorLibro.setText("idAutorLibro");
        nombreLibro.setText("nombreLibro");
        autorLibro.setText("autorLibro");
        costoLibro.setText("costoLibro");
        fechaLibro.setText("fechaLibro");
        generoLibro.setText("generoLibro");
        
        tab.getColumns().addAll(idLibros,idAutorLibro,nombreLibro,autorLibro,costoLibro,fechaLibro,generoLibro);
        
        //agregarLibro 
        hbox1 = new HBox();
        agregarGrid = new GridPane();
        agregarGrid.setHgap(5);
        agregarGrid.setVgap(5);
        
        Label RegistroL;
        RegistroL = new Label("Ingresar Libro");
        RegistroL.setFont(Font.font("Ingresar Libro", FontWeight.BLACK, 20));
        agregarGrid.add(RegistroL,0,1);
        
        Label idAutor;
        idAutor = new Label("IDAutor");
        agregarGrid.add(idAutor,0,2);
          
        idAutor2 = new TextField();
        agregarGrid.add(idAutor2,1,2);
        
        Label nombreLibro1;
        nombreLibro1 = new Label("Nombre");
        agregarGrid.add(nombreLibro1,0,3);
         
        nombreL = new TextField();
        agregarGrid.add(nombreL,1,3);
        
        Label autorLibro1;
        autorLibro1 = new Label("Autor");
        agregarGrid.add(autorLibro1,0,4);
       
        autorL = new TextField();
        agregarGrid.add(autorL,1,4);
        
        Label costoLibro1;
        costoLibro1 = new Label("Costo");
        agregarGrid.add(costoLibro1, 0, 5);
        
        CostoL = new TextField();
        agregarGrid.add(CostoL,1,5);
        
        Label fechaLibro1;
        fechaLibro1 = new Label("Fecha");
        agregarGrid.add(fechaLibro1, 0, 6);
        
        fechaL = new TextField();
        agregarGrid.add(fechaL,1,6);
        
        Label generoLibro1;
        generoLibro1 = new Label("Genero");
        agregarGrid.add(generoLibro1,0,7);
        
        generoL = new TextField();
        agregarGrid.add(generoL,1,7);
        
        Button button = new Button("Agregar");
        agregarGrid.add(button,1,8);
        
        Button button1 = new Button("Modificar");
        agregarGrid.add(button1, 1, 9);
        
        Button button2 = new Button("Eliminar");
        agregarGrid.add(button2,1,10);
        
        button.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
               handlerLibro.getInstancia().agregar(Integer.parseInt(idAutor2.getText()),nombreL.getText(),autorL.getText(),Integer.parseInt(CostoL.getText()),fechaL.getText(),generoL.getText());
               limpiarL();
            }
        });
        
        hbox1.getChildren().add(agregarGrid);
    
        Gridpane.add(tab,0 ,2, 2, 1);
        Gridpane.setPadding(new Insets(20, 20, 20, 20));
        Gridpane.setHgap(75);
        Gridpane.setVgap(15);
        hbOx.getChildren().addAll(Gridpane,hbox1);    
        return hbOx;
    }
    
    public void limpiarL(){
        observableListLibro1.clear();
        handlerLibro.getInstancia().getListaLibro().clear();
        observableListLibro1 =  FXCollections.observableArrayList(handlerLibro.getInstancia().getListaLibro());
        
        tab.setItems(observableListLibro1);
    }
}
    
