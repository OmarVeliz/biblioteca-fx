
package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.omarveliz.bean.Autores;
import org.omarveliz.bean.Libro;
import org.omarveliz.handler.handlerAutores;
import org.omarveliz.handler.handlerLibro;

public class AutoresInt {
   
    private ArrayList<Autores> ArrayListAutores;
    private ObservableList<Autores> observableListAutores; 
    private HBox aut1;
    private HBox aut2;
    private GridPane agregarGrid;
    private TextField idAutorL;
    private TextField idEditorialL;
    private TextField idGeneroAut;
    private TextField nombreAutorL;
    private TextField ColeccionAutor1;
    private TextField generoAutor1;
    private TableView tabA;
    
    public HBox stageLibro(){
    observableListAutores =
                FXCollections.observableArrayList(handlerAutores.getInstancia().getListaAutores());
   
        aut1 = new HBox ();
        GridPane gridAut = new GridPane();
        Text titulo = new Text ("Tabla Autores");
        titulo.setFont(Font.font("Autores", FontWeight.NORMAL, 20));
        gridAut.add(titulo, 0, 0);
        Button buscar = new Button("Buscar");
        gridAut.add(buscar, 1, 1);
        
        TableColumn idAutor = new TableColumn();
        idAutor.setCellValueFactory(new PropertyValueFactory<Autores,Integer>("idAutor"));
        
        TableColumn idEditorial = new TableColumn();
        idEditorial.setCellValueFactory(new PropertyValueFactory<Autores,Integer>("idEditorial"));
        
        TableColumn idGeneroAutor = new TableColumn();
        idGeneroAutor.setCellValueFactory(new PropertyValueFactory<Autores,Integer>("idGeneroAutor"));
        
        TableColumn nombreAutor = new TableColumn();
        nombreAutor.setCellValueFactory(new PropertyValueFactory<Autores,String>("nombreAutor"));
        
        TableColumn ColeccionAutor = new TableColumn ();
        ColeccionAutor.setCellValueFactory(new PropertyValueFactory<Autores,String>("ColeccionAutor"));
        
        TableColumn generoAutor = new TableColumn ();
        generoAutor.setCellValueFactory(new PropertyValueFactory<Autores,String>("generoAutor"));
        
        tabA = new TableView <> (observableListAutores);
        idAutor.setText("idAutor");
        idEditorial.setText("idEditorial");
        idGeneroAutor.setText("idGeneroAutor");
        nombreAutor.setText("nombreAutor");
        ColeccionAutor.setText("ColeccionAutor");
        generoAutor.setText("generoAutor");
        
        tabA.getColumns().addAll(idAutor,idEditorial,idGeneroAutor,nombreAutor,ColeccionAutor,generoAutor);
        
        //agregarLibro 
        aut2 = new HBox();
        agregarGrid = new GridPane();
        agregarGrid.setHgap(5);
        agregarGrid.setVgap(5);
        
        Label RegistroL;
        RegistroL = new Label("Ingresar Autor");
        RegistroL.setFont(Font.font("Ingresar Autor", FontWeight.BLACK, 20));
        agregarGrid.add(RegistroL,0,1);
        
        Label idAutor1;
        idAutor1 = new Label("ID Autor(Modificar)");
        agregarGrid.add(idAutor1,0,2);
          
        idAutorL = new TextField();
        agregarGrid.add(idAutorL,1,2);
        
        Label IdEditorialLib;
        IdEditorialLib = new Label("ID Editorial");
        agregarGrid.add(IdEditorialLib,0,3);
         
        idEditorialL = new TextField();
        agregarGrid.add(idEditorialL,1,3);
        
        Label idGeneroAut1;
        idGeneroAut1 = new Label("ID genero autor");
        agregarGrid.add(idGeneroAut1,0,4);
       
        idGeneroAut = new TextField();
        agregarGrid.add(idGeneroAut,1,4);
        
        Label nombreAut1;
        nombreAut1 = new Label("Nombre Autor");
        agregarGrid.add(nombreAut1, 0, 5);
       
        nombreAutorL = new TextField();
        agregarGrid.add(nombreAutorL,1,5);
        
        Label ColeccionAutor12;
        ColeccionAutor12 = new Label("Coleccion");
        agregarGrid.add(ColeccionAutor12, 0, 6);
        
        ColeccionAutor1 = new TextField();
        agregarGrid.add(ColeccionAutor1,1,6);
        
        Label generoAutor12;
        generoAutor12 = new Label("generoAutor12");
        agregarGrid.add(generoAutor12,0,7);
        
        generoAutor1 = new TextField();
        agregarGrid.add(generoAutor1,1,7);
        
        Button button = new Button("Agregar");
        agregarGrid.add(button,1,8);
        
        button.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
               handlerAutores.getInstancia().agregar(Integer.parseInt(idEditorialL.getText()),Integer.parseInt(idGeneroAut.getText()),nombreAutorL.getText(),ColeccionAutor1.getText(),generoAutor1.getText());
               limpiarL();
            }
        });
        
        aut2.getChildren().add(agregarGrid);
    
        gridAut.add(tabA,0 ,2, 2, 1);
        gridAut.setPadding(new Insets(20, 20, 20, 20));
        gridAut.setHgap(75);
        gridAut.setVgap(15);
        aut1.getChildren().addAll(gridAut,aut2);    
        return aut1;
    }
    
    public void limpiarL(){
        observableListAutores.clear();
        handlerAutores.getInstancia().getListaAutores().clear();
        observableListAutores =  FXCollections.observableArrayList(handlerAutores.getInstancia().getListaAutores());
        
        tabA.setItems(observableListAutores);
    }
}
