CREATE PROCEDURE agregarL
@nombreLibro VARCHAR(255),
@autorLibro VARCHAR(255),
@costoLibro INT,
@fechaLibro VARCHAR(255),
@generoLibro VARCHAR(255)
AS
BEGIN
	INSERT INTO Libros(nombreLibro,autorLibro,costoLibro,fechaLibro,generoLibro) 
	VALUES (@nombreLibro, @autorLibro, @costoLibro, @fechaLibro, @generoLibro);
END
GO

EXECUTE agregarL 'INFERNO','Dan brown',100,'11/3/14','Novela'

SELECT * FROM Libros;

