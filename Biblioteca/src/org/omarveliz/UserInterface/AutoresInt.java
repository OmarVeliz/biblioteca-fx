
package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import org.omarveliz.bean.Autores;
import org.omarveliz.bean.Genero;
import org.omarveliz.bean.Libro;
import org.omarveliz.handler.handlerAutores;
import org.omarveliz.handler.handlerGenero;
import org.omarveliz.handler.handlerLibro;

public class AutoresInt {
   
    private ArrayList<Autores> ArrayListAutores;
    private ObservableList<Autores> observableListAutores; 
    private HBox aut1;
    private HBox aut2;
    private GridPane agregarGrid;
    private TextField idAutorL;
    private TextField idEditorialL;
    private TextField idGeneroAut;
    private TextField nombreAutorL;
    private TextField ColeccionAutor1;
    private TextField generoAutor1;
    private TableView <Autores> tabA;
    
    public HBox stageLibro(){
    observableListAutores =
                FXCollections.observableArrayList(handlerAutores.getInstancia().getListaAutores());
   
        aut1 = new HBox ();
        GridPane gridAut = new GridPane();
        Text titulo = new Text ("Tabla Autores");
        titulo.setFont(Font.font("Autores", FontWeight.NORMAL, 20));
        gridAut.add(titulo, 0, 0);
        Button buscar = new Button("Buscar");
        gridAut.add(buscar, 1, 1);
        
        TableColumn idAutor = new TableColumn();
        idAutor.setCellValueFactory(new PropertyValueFactory<Autores,Integer>("idAutor"));
        
        TableColumn idEditorial = new TableColumn();
        idEditorial.setCellValueFactory(new PropertyValueFactory<Autores,Integer>("idEditorial"));
        
        TableColumn idGeneroAutor = new TableColumn();
        idGeneroAutor.setCellValueFactory(new PropertyValueFactory<Autores,Integer>("idGeneroAutor"));
        
        TableColumn nombreAutor = new TableColumn();
        nombreAutor.setCellValueFactory(new PropertyValueFactory<Autores,String>("nombreAutor"));
        
        TableColumn ColeccionAutor = new TableColumn ();
        ColeccionAutor.setCellValueFactory(new PropertyValueFactory<Autores,String>("ColeccionAutor"));
        
        tabA = new TableView <> (observableListAutores);
        idAutor.setText("idAutor");
        idEditorial.setText("idEditorial");
        idGeneroAutor.setText("idGeneroAutor");
        nombreAutor.setText("nombreAutor");
        ColeccionAutor.setText("ColeccionAutor");
        
        tabA.getColumns().addAll(idAutor,idEditorial,idGeneroAutor,nombreAutor,ColeccionAutor);
        
        //agregarLibro 
        aut2 = new HBox();
        agregarGrid = new GridPane();
        agregarGrid.setHgap(5);
        agregarGrid.setVgap(5);
        
        Label RegistroL;
        RegistroL = new Label("Ingresar Autor");
        RegistroL.setFont(Font.font("Ingresar Autor", FontWeight.BLACK, 20));
        agregarGrid.add(RegistroL,0,1);
        
        final Label idAutor1;
        idAutor1 = new Label("ID Autor(Modificar)");
        agregarGrid.add(idAutor1,0,2);
          
        idAutor1.setVisible(false);
      
        idAutorL = new TextField();
        agregarGrid.add(idAutorL,1,2);
        
        idAutorL.setPromptText("ID Autor");
        idAutorL.setVisible(false);
        
        Label IdEditorialLib;
        IdEditorialLib = new Label("ID Editorial");
        agregarGrid.add(IdEditorialLib,0,3);
         
        idEditorialL = new TextField();
        agregarGrid.add(idEditorialL,1,3);
        
        idEditorialL.setPromptText("ID Editorial");
        
        Label idGeneroAut1;
        idGeneroAut1 = new Label("ID genero autor");
        agregarGrid.add(idGeneroAut1,0,4);
       
        idGeneroAut = new TextField();
        agregarGrid.add(idGeneroAut,1,4);
        
        idGeneroAut.setPromptText("ID Genero");
        
        Label nombreAut1;
        nombreAut1 = new Label("Nombre Autor");
        agregarGrid.add(nombreAut1, 0, 5);
       
        nombreAutorL = new TextField();
        agregarGrid.add(nombreAutorL,1,5);
       
        nombreAutorL.setPromptText("Nombre");
        
        Label ColeccionAutor12;
        ColeccionAutor12 = new Label("Coleccion");
        agregarGrid.add(ColeccionAutor12, 0, 6);
        
        ColeccionAutor1 = new TextField();
        agregarGrid.add(ColeccionAutor1,1,6);
        
        ColeccionAutor1.setPromptText("Coleccion");
      
        final Button button = new Button("Agregar");
        agregarGrid.add(button,1,7);
        
        final Button salirAut = new Button("Salir Modificar");
        agregarGrid.add(salirAut,1,9);
        
        final Button modificarA = new Button("Modificar");
        agregarGrid.add(modificarA,1,8);
        
        salirAut.setVisible(false);
        modificarA.setVisible(false);
        
        button.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
               handlerAutores.getInstancia().agregar(Integer.parseInt(idAutorL.getText()),Integer.parseInt(idGeneroAut.getText()),nombreAutorL.getText(),ColeccionAutor1.getText());
               limpiarL();
            }
        });
        
        
        
        tabA.setOnMouseClicked(new EventHandler<MouseEvent>(){
           
            @Override
            public void handle(MouseEvent t) {
               
                if (tabA.getSelectionModel().getSelectedItem() != null) {
                
                     idAutorL.setVisible(true);
                     button.setVisible(false);
                     salirAut.setVisible(true);
                     idAutor1.setVisible(true);
                     idAutorL.setVisible(true);
                     modificarA.setVisible(true);
                  
                Autores autores = (Autores)tabA.getSelectionModel().getSelectedItem();
                
                idAutorL.setText(Integer.toString(autores.getIdAutor()));
                idEditorialL.setText(Integer.toString(autores.getIdEditorial()));
                idGeneroAut.setText(Integer.toString(autores.getIdGeneroAutor()));
                nombreAutorL.setText(autores.getNombreAutor());
                ColeccionAutor1.setText(autores.getColeccionAutor());
                
                } else {
                    System.out.println("Lugar no valido");
                }
                    
            }    
            
        });
        
        
        salirAut.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                button.setVisible(true);
                modificarA.setVisible(false);
                salirAut.setVisible(true);
                idAutor1.setVisible(false);
                idAutorL.setVisible(false);
                
                idAutor1.setText(null);
                idEditorialL.setText(null);
                idGeneroAut.setText(null);
                nombreAutorL.setText(null);
                ColeccionAutor1.setText(null);
      
            }
        });
        
        modificarA.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
              handlerAutores.getInstancia().modificar(Integer.parseInt(idAutorL.getText()), Integer.parseInt(idEditorialL.getText()), Integer.parseInt(idGeneroAut.getText()),nombreAutorL.getText(), ColeccionAutor1.getText());
              limpiarL();
            }
        });
        
        final ContextMenu contextMenu;
        contextMenu = new ContextMenu();
        
    contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Eliminar");
        }   
    });
    
    contextMenu.setOnShown(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Cambiar");
        }
    });

    MenuItem item1 = new MenuItem("Eliminar");
    item1.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            if (tabA.getSelectionModel().getSelectedItem() != null) {
                handlerAutores.getInstancia().eliminar(tabA.getSelectionModel().getSelectedItem().getIdAutor());
                limpiarL();
            }
        }
    });
    MenuItem item2 = new MenuItem("Preferences");
    item2.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            System.out.println("Preferences");
        }
    });
    contextMenu.getItems().addAll(item1, item2);
    
    tabA.setContextMenu(contextMenu);
      
        aut2.getChildren().add(agregarGrid);
    
        gridAut.add(tabA,0 ,2, 2, 1);
        gridAut.setPadding(new Insets(20, 20, 20, 20));
        gridAut.setHgap(75);
        gridAut.setVgap(15);
        aut1.getChildren().addAll(gridAut,aut2);    
        return aut1;
    }
    
    public void limpiarL(){
        observableListAutores.clear();
        handlerAutores.getInstancia().getListaAutores().clear();
        observableListAutores =  FXCollections.observableArrayList(handlerAutores.getInstancia().getListaAutores());
        
        tabA.setItems(observableListAutores);
    }
}
