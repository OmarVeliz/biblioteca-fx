
package org.omarveliz.UserInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import org.omarveliz.bean.Autores;
import org.omarveliz.bean.Prestamo;
import org.omarveliz.handler.handlerAutores;
import org.omarveliz.handler.handlerPrestamos;

public class PrestamoInt {
    private ArrayList<Prestamo> ArrayListPrest;
    private ObservableList<Prestamo> observableListPrest; 
    private HBox prest1;
    private HBox prest2;
    private TextField CampIdPr;
    private TextField CampIdUsua;
    private TextField CampIdLibr;
    private TextField campFechaPrest;
    private TextField campFechaDevoc;
    private TableView <Prestamo> tabP;
    
    public HBox stagePrestamo(){
      observableListPrest =
              FXCollections.observableArrayList(handlerPrestamos.getInstancia().getListaPrestamo());
      
      prest1 = new HBox();
      GridPane gridPrest = new GridPane();
      Text titulo = new Text ("Tabla Prestamos");
      titulo.setFont(Font.font("Prestamos", FontWeight.NORMAL, 20));
      gridPrest.add(titulo, 0, 0);
      Button buscar = new Button("Buscar");
      gridPrest.add(buscar, 1, 1);
      
      TableColumn idPrestamo = new TableColumn();
      idPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo,Integer>("idPrestamo"));
      
      TableColumn idUsuario = new TableColumn();
      idUsuario.setCellValueFactory(new PropertyValueFactory<Prestamo,Integer>("idUsuario"));
        
      TableColumn idLibro = new TableColumn();
      idLibro.setCellValueFactory(new PropertyValueFactory<Prestamo,Integer>("idLibro"));
      
      TableColumn fechaPrestamo = new TableColumn();
      fechaPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("fechaPrestamo"));
      
      TableColumn fechaDevolucion = new TableColumn();
      fechaDevolucion.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("fechaDevolucion"));
      
      tabP = new TableView <> (observableListPrest);
      idPrestamo.setText("idPrestamo");
      idUsuario.setText("idUsuario");
      idLibro.setText("idLibro");
      fechaPrestamo.setText("fechaPrestamo");
      fechaDevolucion.setText("fechaDevolucion");
         
      tabP.getColumns().addAll(idPrestamo,idUsuario,idLibro,fechaPrestamo,fechaDevolucion);
      
      //agregarEditorial
      
      prest2 = new HBox();
      GridPane agregarPrest12 = new GridPane();
      agregarPrest12.setVgap(5);
      agregarPrest12.setHgap(5);
      
       Label RegistroL;
        RegistroL = new Label("Ingresar Prestamo");
        RegistroL.setFont(Font.font("Ingresar Prestamo", FontWeight.BLACK, 20));
        agregarPrest12.add(RegistroL,0,1);
        
        final Label idPrestamoC;
        idPrestamoC = new Label("ID Prestamo(Modificar)");
        agregarPrest12.add(idPrestamoC,0,2);
          
        idPrestamoC.setVisible(false);
        
        CampIdPr = new TextField();
        agregarPrest12.add(CampIdPr,1,2);
        
        CampIdPr.setVisible(false);
        
        Label idUsuarioP;
        idUsuarioP = new Label("ID Usuario");
        agregarPrest12.add(idUsuarioP,0,3);
                 
        CampIdUsua = new TextField();
        agregarPrest12.add(CampIdUsua,1,3);
        
        CampIdUsua.setPromptText("ID Usuario");
        
        Label idLibro12;
        idLibro12 = new Label("ID Libro");
        agregarPrest12.add(idLibro12,0,4);
       
        CampIdLibr = new TextField();
        agregarPrest12.add(CampIdLibr,1,4);
        
        CampIdLibr.setPromptText("ID Libro");
        
        Label nombreGener1;
        nombreGener1 = new Label("Fecha Prestamo");
        agregarPrest12.add(nombreGener1,0,5);
        
        campFechaPrest = new TextField();
        agregarPrest12.add(campFechaPrest,1,5);
        
        campFechaPrest.setPromptText("Fecha Prestamo");
        
        Label fechaEntreg;
        fechaEntreg = new Label("Fecha Entrega");
        agregarPrest12.add(fechaEntreg,0,6);
        
        campFechaDevoc = new TextField();
        agregarPrest12.add(campFechaDevoc,1,6);
         
        campFechaDevoc.setPromptText("Fecha Entrega");
        
        final Button agregarEdit12 = new Button("Agregar");
        agregarPrest12.add(agregarEdit12,1,7);
        
        Button mora = new Button("Cuota a pagar");
        agregarPrest12.add(mora,1,9);
        
        final Button salirPrest = new Button("Salir Prestamo");
        agregarPrest12.add(salirPrest,1,10);
        
        final Button modificarP = new Button("Modificar");
        agregarPrest12.add(modificarP,1,8);
        
        agregarEdit12.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                handlerPrestamos.getInstancia().agregar(Integer.parseInt(CampIdUsua.getText()),Integer.parseInt(CampIdLibr.getText()),campFechaPrest.getText(), campFechaDevoc.getText());
                LimpiarL();
            }
        });
        
        
        mora.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                    
                String cadena  = campFechaPrest.getText();
                String cadena2 = campFechaDevoc.getText();
               
            }
        });
        
          
        tabP.setOnMouseClicked(new EventHandler<MouseEvent>(){
           
            @Override
            public void handle(MouseEvent t) {
               
                if (tabP.getSelectionModel().getSelectedItem() != null) {
                    
                    agregarEdit12.setVisible(false);
                    idPrestamoC.setVisible(true);
                    CampIdPr.setVisible(true);
                     
                    salirPrest.setVisible(true);  
                    modificarP.setVisible(true);
                  
                Prestamo prestamo = (Prestamo)tabP.getSelectionModel().getSelectedItem();
                
                CampIdPr.setText(Integer.toString(prestamo.getIdPrestamo()));
                CampIdUsua.setText(Integer.toString(prestamo.getIdUsuario()));
                CampIdLibr.setText(Integer.toString(prestamo.getIdPrestamo()));
                campFechaPrest.setText(prestamo.getFechaPrestamo());
                campFechaDevoc.setText(prestamo.getFechaDevolucion());
                
                } else {
                    System.out.println("Lugar no valido");
                }
                    
            }    
            
        });
        
        
        salirPrest.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                agregarEdit12.setVisible(true);
                modificarP.setVisible(false);
                salirPrest.setVisible(true);
                idPrestamoC.setVisible(false);
                CampIdPr.setVisible(false);
                
                CampIdPr.setText(null);
                CampIdUsua.setText(null);
                CampIdLibr.setText(null);
                campFechaPrest.setText(null);
                campFechaDevoc.setText(null);
      
            }
        });
        
        modificarP.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
              handlerPrestamos.getInstancia().modificar(Integer.parseInt(CampIdPr.getText()),Integer.parseInt(CampIdUsua.getText()),Integer.parseInt(CampIdLibr.getText()),campFechaPrest.getText(), campFechaDevoc.getText());
              LimpiarL();
            }
        });
        
        final ContextMenu contextMenu;
        contextMenu = new ContextMenu();
        
    contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Eliminar");
        }   
    });
    
    contextMenu.setOnShown(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Cambiar");
        }
    });

    MenuItem item1 = new MenuItem("Eliminar");
    item1.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            if (tabP.getSelectionModel().getSelectedItem() != null) {
                handlerPrestamos.getInstancia().eliminarP(tabP.getSelectionModel().getSelectedItem().getIdPrestamo());
                LimpiarL();
            }
        }
    });
    MenuItem item2 = new MenuItem("Preferences");
    item2.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            System.out.println("Preferences");
        }
    });
    contextMenu.getItems().addAll(item1, item2);
    
    tabP.setContextMenu(contextMenu);
      
        prest2.getChildren().addAll(agregarPrest12);
        gridPrest.add(tabP,0,2,2,1);
        gridPrest.setPadding(new Insets(20, 20, 20, 20));
        gridPrest.setVgap(15);
        gridPrest.setHgap(75);
        prest1.getChildren().addAll(gridPrest,prest2);
        return prest1;
    }
    
    public void LimpiarL() {
        observableListPrest.clear();
        handlerPrestamos.getInstancia().getListaPrestamo().clear();
        observableListPrest = FXCollections.observableArrayList(handlerPrestamos.getInstancia().getListaPrestamo());
        
        tabP.setItems(observableListPrest);
    
    }
    
    public long getDiffDates(Date fechaInicio, Date fechaFin, int tipo) {
	// Fecha inicio
        
        
	Calendar calendarInicio = Calendar.getInstance();
	calendarInicio.setTime(fechaInicio);
	int diaInicio = calendarInicio.get(Calendar.DAY_OF_MONTH);
	int mesInicio = calendarInicio.get(Calendar.MONTH) + 1; // 0 Enero, 11 Diciembre
	int anioInicio = calendarInicio.get(Calendar.YEAR);

	// Fecha fin
	Calendar calendarFin = Calendar.getInstance();
	calendarFin.setTime(fechaFin);
	int diaFin = calendarFin.get(Calendar.DAY_OF_MONTH);
	int mesFin = calendarFin.get(Calendar.MONTH) + 1; // 0 Enero, 11 Diciembre
	int anioFin = calendarFin.get(Calendar.YEAR);

	int anios = 0;
	int mesesPorAnio = 0;
	int diasPorMes = 0;
	int diasTipoMes = 0;
	
	//
	// Calculo de días del mes
	//
	if (mesInicio == 2) {
		// Febrero
		if ((anioFin % 4 == 0) && ((anioFin % 100 != 0) || (anioFin % 400 == 0))) {
			// Bisiesto
			diasTipoMes = 29;
		} else {
			// No bisiesto
			diasTipoMes = 28;
		}
	} else if (mesInicio <= 7) {
		// De Enero a Julio los meses pares tienen 30 y los impares 31
		if (mesInicio % 2 == 0) {
			diasTipoMes = 30;
		} else {
			diasTipoMes = 31;
		}
	} else if (mesInicio > 7) {
		// De Julio a Diciembre los meses pares tienen 31 y los impares 30
		if (mesInicio % 2 == 0) {
			diasTipoMes = 31;
		} else {
			diasTipoMes = 30;
		}
	}
	
	
	//
	// Calculo de diferencia de año, mes y dia
	//
	if ((anioInicio > anioFin) || (anioInicio == anioFin && mesInicio > mesFin)
			|| (anioInicio == anioFin && mesInicio == mesFin && diaInicio > diaFin)) {
		// La fecha de inicio es posterior a la fecha fin
		// System.out.println("La fecha de inicio ha de ser anterior a la fecha fin");
		return -1;			
	} else {
		if (mesInicio <= mesFin) {
			anios = anioFin - anioInicio;
			if (diaInicio <= diaFin) {
				mesesPorAnio = mesFin - mesInicio;
				diasPorMes = diaFin - diaInicio;
			} else {
				if (mesFin == mesInicio) {
					anios = anios - 1;
				}
				mesesPorAnio = (mesFin - mesInicio - 1 + 12) % 12;
				diasPorMes = diasTipoMes - (diaInicio - diaFin);
			}
		} else {
			anios = anioFin - anioInicio - 1;
			System.out.println(anios);
			if (diaInicio > diaFin) {
				mesesPorAnio = mesFin - mesInicio - 1 + 12;
				diasPorMes = diasTipoMes - (diaInicio - diaFin);
			} else {
				mesesPorAnio = mesFin - mesInicio + 12;
				diasPorMes = diaFin - diaInicio;
			}
		}
	}
	//System.out.println("Han transcurrido " + anios + " Años, " + mesesPorAnio + " Meses y " + diasPorMes + " Días.");		

	//
	// Totales
	//
	long returnValue = -1;
	
	switch (tipo) {
		case 0:
			// Total Años
			returnValue = anios;
			System.out.println("Total años: " + returnValue + " Años.");
			break;
		
		case 1:
			// Total Meses
			returnValue = anios * 12 + mesesPorAnio;
			System.out.println("Total meses: " + returnValue + " Meses.");
			break;
			
		case 2:
			// Total Dias (se calcula a partir de los milisegundos por día)
			long millsecsPerDay = 86400000; // Milisegundos al día
			returnValue = (fechaFin.getTime() - fechaInicio.getTime()) / millsecsPerDay;
			System.out.println("Total días: " + returnValue + " Días.");
			break;
			
		case 3:
			// Meses del año
			returnValue = mesesPorAnio;
			System.out.println("Meses del año: " + returnValue);
			break;
		
		case 4:
			// Dias del mes
			returnValue = diasPorMes;
			System.out.println("Dias del mes: " + returnValue);
			break;

		default:
			break;
	}
	
	return returnValue;
}
    
}
