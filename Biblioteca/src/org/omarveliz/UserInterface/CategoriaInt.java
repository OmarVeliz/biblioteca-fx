package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.omarveliz.bean.Categoria;
import org.omarveliz.bean.Editorial;
import org.omarveliz.handler.handlerCategoria;
import org.omarveliz.handler.handlerEditorial;

public class CategoriaInt {
    private ArrayList<Categoria> ArrayListCat;
    private ObservableList<Categoria> observableListCat; 
    private HBox cat1;
    private HBox Cat2;
    private TextField CampIdC;
    private TextField CamIDLib;
    private TextField CamNombreC;
    private TextField campNombG;
    private TextField campAutorCat;
    private TableView tabA;
    
    
    public HBox stageCategoria(){
      observableListCat =
              FXCollections.observableArrayList(handlerCategoria.getInstancia().getListaCategoria());
      
      cat1 = new HBox();
      GridPane gridCat = new GridPane();
      Text titulo = new Text ("Tabla Categoria");
      titulo.setFont(Font.font("Categoria", FontWeight.NORMAL, 20));
      gridCat.add(titulo, 0, 0);
      Button buscar = new Button("Buscar");
      gridCat.add(buscar, 1, 1);
      
      TableColumn idCategoria = new TableColumn();
      idCategoria.setCellValueFactory(new PropertyValueFactory<Categoria,Integer>("idCategoria"));
      
      TableColumn idLibro = new TableColumn();
      idLibro.setCellValueFactory(new PropertyValueFactory<Categoria,Integer>("idLibro"));
        
      TableColumn nombreCategoria = new TableColumn();
      nombreCategoria.setCellValueFactory(new PropertyValueFactory<Categoria,String>("nombreCategoria"));
      
      TableColumn nombreGenero1 = new TableColumn();
      nombreGenero1.setCellValueFactory(new PropertyValueFactory<Categoria,String>("nombreGenero1"));
      
      TableColumn autoresCategoria = new TableColumn();
      autoresCategoria.setCellValueFactory(new PropertyValueFactory<Categoria,String>("autoresCategoria"));
      
      tabA = new TableView <> (observableListCat);
      idCategoria.setText("idCategoria");
      idLibro.setText("idLibro");
      nombreCategoria.setText("nombreCategoria");
      nombreGenero1.setText("nombreGenero1");
      autoresCategoria.setText("autoresCategoria");
         
      tabA.getColumns().addAll(idCategoria,idLibro,nombreCategoria,nombreGenero1,autoresCategoria);
      
      //agregarEditorial
      
      Cat2 = new HBox();
      GridPane agregarCat12 = new GridPane();
      agregarCat12.setVgap(5);
      agregarCat12.setHgap(5);
      
       Label RegistroL;
        RegistroL = new Label("Ingresar Categoria");
        RegistroL.setFont(Font.font("Ingresar Categoria", FontWeight.BLACK, 20));
        agregarCat12.add(RegistroL,0,1);
        
        Label idCategoriaC;
        idCategoriaC = new Label("ID Categoria(Modificar)");
        agregarCat12.add(idCategoriaC,0,2);
          
        CampIdC = new TextField();
        agregarCat12.add(CampIdC,1,2);
        
        Label idLibro12;
        idLibro12 = new Label("ID Libro");
        agregarCat12.add(idLibro12,0,3);
         
        CamIDLib = new TextField();
        agregarCat12.add(CamIDLib,1,3);
        
        Label nombreCat1;
        nombreCat1 = new Label("Nombre Categoria");
        agregarCat12.add(nombreCat1,0,4);
       
        CamNombreC = new TextField();
        agregarCat12.add(CamNombreC,1,4);
        
        Label nombreGener1;
        nombreGener1 = new Label("nombre Genero");
        agregarCat12.add(nombreGener1,0,5);
        
        campNombG = new TextField();
        agregarCat12.add(campNombG,1,5);
        
        Label nombreAutoresC;
        nombreAutoresC = new Label("Autor");
        agregarCat12.add(nombreAutoresC,0,6);
        
        campAutorCat = new TextField();
        agregarCat12.add(campAutorCat,1,6);
         
        Button agregarEdit12 = new Button("Agregar");
        agregarCat12.add(agregarEdit12,1,7);
        
        agregarEdit12.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                handlerCategoria.getInstancia().Agregar(Integer.parseInt(CamIDLib.getText()), CamNombreC.getText(), campNombG.getText(), campAutorCat.getText());
                LimpiarL();
            }
        });
        
        
        Cat2.getChildren().addAll(agregarCat12);
        gridCat.add(tabA,0,2,2,1);
        gridCat.setPadding(new Insets(20, 20, 20, 20));
        gridCat.setVgap(15);
        gridCat.setHgap(75);
        cat1.getChildren().addAll(gridCat,Cat2);
        return cat1;
    }
    
    public void LimpiarL() {
        observableListCat.clear();
        handlerCategoria.getInstancia().getListaCategoria().clear();
        observableListCat = FXCollections.observableArrayList(handlerCategoria.getInstancia().getListaCategoria());
        
        tabA.setItems(observableListCat);
    
    }
    
}
