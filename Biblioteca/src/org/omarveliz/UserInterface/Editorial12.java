
package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import org.omarveliz.handler.handlerEditorial;
import org.omarveliz.bean.Editorial;
import org.omarveliz.bean.Genero;
import org.omarveliz.handler.handlerGenero;

public class Editorial12 {
    
    private ArrayList<Editorial> ArrayListEdit;
    private ObservableList<Editorial> observableListEdit1; 
    private HBox hbOx2;
    private HBox hbox3;
    private GridPane agregarGrid;
    private TextField idEditorial2;
    private TextField nombreEdit;
    private TextField distribuidores2;
    private TableView <Editorial> tab1;
    
     public HBox stageEditorial(){
      observableListEdit1 =
              FXCollections.observableArrayList(handlerEditorial.getInstancia().getListaEditorial());
      
      hbOx2 = new HBox();
      GridPane gridEdit = new GridPane();
      Text titulo = new Text ("Tabla Editorial");
      titulo.setFont(Font.font("Editoriales", FontWeight.NORMAL, 20));
      gridEdit.add(titulo, 0, 0);
      Button buscar = new Button("Buscar");
      gridEdit.add(buscar, 1, 1);
      
      TableColumn idEditorial = new TableColumn();
      idEditorial.setCellValueFactory(new PropertyValueFactory<Editorial,Integer>("idEditorial"));
        
      TableColumn nombreEditorial = new TableColumn();
      nombreEditorial.setCellValueFactory(new PropertyValueFactory<Editorial,String>("nombreEditorial"));
        
      TableColumn distribuidores = new TableColumn();
      distribuidores.setCellValueFactory(new PropertyValueFactory<Editorial,String>("distribuidores"));
         
      tab1 = new TableView <> (observableListEdit1);
      idEditorial.setText("IdEditorial");
      nombreEditorial.setText("nombreEditorial");
      distribuidores.setText("distribuidores");
         
      tab1.getColumns().addAll(idEditorial,nombreEditorial,distribuidores);
      
      //agregarEditorial
      
      hbox3 = new HBox();
      GridPane agregarEdit1 = new GridPane();
      agregarEdit1.setVgap(5);
      agregarEdit1.setHgap(5);
      
       Label RegistroL;
        RegistroL = new Label("Ingresar Editorial");
        RegistroL.setFont(Font.font("Ingresar Editorial", FontWeight.BLACK, 20));
        agregarEdit1.add(RegistroL,0,1);
        
        final Label idEditorial1;
        idEditorial1 = new Label("idEditorial");
        agregarEdit1.add(idEditorial1,0,2);
          
        idEditorial1.setVisible(false);
        
        idEditorial2 = new TextField();
        agregarEdit1.add(idEditorial2,1,2);
        
        idEditorial2.setVisible(false);
        
        idEditorial2.setPromptText("ID Editorial");
        
        Label nombreEditorial1;
        nombreEditorial1 = new Label("Nombre Editorial");
        agregarEdit1.add(nombreEditorial1,0,3);
         
        nombreEdit = new TextField();
        agregarEdit1.add(nombreEdit,1,3);
        
        nombreEdit.setPromptText("Nombre Editorial");
        
        Label distribuidores1;
        distribuidores1 = new Label("Distribuidores");
        agregarEdit1.add(distribuidores1,0,4);
       
        distribuidores2 = new TextField();
        agregarEdit1.add(distribuidores2,1,4);
        
        distribuidores2.setPromptText("Distribuidores");
        
        final Button agregarEdit12 = new Button("Agregar");
        agregarEdit1.add(agregarEdit12,1,5);
        
        final Button salirModif1 = new Button("Salir Agregar");
        agregarEdit1.add(salirModif1,1,9);
       
        final Button modificarE = new Button("Modificar");
        agregarEdit1.add(modificarE,1,6);
        
        modificarE.setVisible(false);
        
        agregarEdit12.setVisible(true);
        agregarEdit12.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
              handlerEditorial.getInstancia().agregarEdit(nombreEdit.getText() ,distribuidores2.getText());
               limpiarL();
            }
        });
        
        
        tab1.setOnMouseClicked(new EventHandler<MouseEvent>(){
           
            @Override
            public void handle(MouseEvent t) {
               
                if (tab1.getSelectionModel().getSelectedItem() != null) {
                agregarEdit12.setVisible(false);
                idEditorial1.setVisible(true);
                idEditorial2.setVisible(true);
                modificarE.setVisible(true);
                  
                Editorial editorial = (Editorial)tab1.getSelectionModel().getSelectedItem();
                
                idEditorial2.setText(Integer.toString(editorial.getIdEditorial()));
                nombreEdit.setText(editorial.getNombreEditorial());
                distribuidores2.setText(editorial.getDistribuidores());
                
                } else {
                    System.out.println("Lugar no valido");
                }
                    
            }    
            
        });
        
        
        salirModif1.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                agregarEdit12.setVisible(true);
                idEditorial1.setVisible(false);
                idEditorial2.setVisible(false);
                
                
                idEditorial1.setText(null);
                idEditorial2.setText(null);
                nombreEdit.setText(null);
                distribuidores2.setText(null);
            }
        });
        
        modificarE.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
              handlerEditorial.getInstancia().modificarEdit(Integer.parseInt(idEditorial2.getText()),nombreEdit.getText(),distribuidores2.getText());
              limpiarL();
            }
        });
        
        final ContextMenu contextMenu;
        contextMenu = new ContextMenu();
        
    contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Eliminar");
        }   
    });
    
    contextMenu.setOnShown(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Cambiar");
        }
    });

    MenuItem item1 = new MenuItem("Eliminar");
    item1.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            if (tab1.getSelectionModel().getSelectedItem() != null) {
                handlerEditorial.getInstancia().eliminarE(tab1.getSelectionModel().getSelectedItem().getIdEditorial());
                limpiarL();
            }
        }
    });
    
    MenuItem item2 = new MenuItem("Preferences");
    item2.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            System.out.println("Preferences");
        }
    });
    contextMenu.getItems().addAll(item1, item2);
    
    tab1.setContextMenu(contextMenu);
        
        hbox3.getChildren().addAll(agregarEdit1);
        gridEdit.add(tab1,0,2,2,1);
        gridEdit.setPadding(new Insets(20, 20, 20, 20));
        gridEdit.setVgap(15);
        gridEdit.setHgap(75);
        hbOx2.getChildren().addAll(gridEdit,hbox3);
        return hbOx2;
    }
     
     public void limpiarL(){
        observableListEdit1.clear();
        handlerEditorial.getInstancia().getListaEditorial().clear();
        observableListEdit1 =  FXCollections.observableArrayList(handlerEditorial.getInstancia().getListaEditorial());
        
        tab1.setItems(observableListEdit1);
    }
     
}
