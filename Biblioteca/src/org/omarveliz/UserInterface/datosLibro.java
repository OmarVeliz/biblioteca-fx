
package org.omarveliz.UserInterface;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import org.omarveliz.bean.Autores;
import org.omarveliz.bean.Libro;
import org.omarveliz.handler.handlerAutores;
import org.omarveliz.handler.handlerLibro;

public class datosLibro {
    
    private ArrayList<Libro> ArrayListLibro;
    private ObservableList<Libro> observableListLibro1; 
    private HBox hbOx;
    private HBox hbox1;
    private GridPane agregarGrid;
    private TextField CampIdLibros;
    private TextField CampIdAutorLibro;
    private TextField CampIdGenero;
    private TextField CampIdEditorial;
    private TextField CampNombreLibro;
    private TextField CampEstadoLibro;
    
    private TableView <Libro> tab;
    
    public HBox stageLibro(){
    observableListLibro1 =
                FXCollections.observableArrayList(handlerLibro.getInstancia().getListaLibro());
   
        hbOx = new HBox ();
        GridPane Gridpane = new GridPane();
        Text titulo = new Text ("Tabla Libro");
        titulo.setFont(Font.font("Libro", FontWeight.NORMAL, 20));
        Gridpane.add(titulo, 0, 0);
        Button buscar = new Button("Buscar");
        Gridpane.add(buscar, 1, 1);
        
        TableColumn idLibros = new TableColumn();
        idLibros.setCellValueFactory(new PropertyValueFactory<Libro,Integer>("idLibros"));
        
        TableColumn nombreAutor = new TableColumn();
        nombreAutor.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreAutor"));
        
        TableColumn nombreGenero = new TableColumn();
        nombreGenero.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreGenero"));
        
        TableColumn nombreEditorial = new TableColumn();
        nombreEditorial.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreEditorial"));
        
        TableColumn nombreLibro = new TableColumn ();
        nombreLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreLibro"));
             
        TableColumn estadoLibro = new TableColumn ();
        estadoLibro.setCellValueFactory(new PropertyValueFactory<Libro,String>("estadoLibro"));
        
        tab = new TableView <> (observableListLibro1);
        idLibros.setText("idLibros");
        nombreAutor.setText("nombreAutor");
        nombreGenero.setText("nombreGenero");
        nombreEditorial.setText("nombreEditorial");
        nombreLibro.setText("nombreLibro");
        estadoLibro.setText("estadoLibro");
        
        tab.getColumns().addAll(idLibros,nombreAutor,nombreGenero,nombreEditorial,nombreLibro,estadoLibro);
        
        //agregarLibro 
        hbox1 = new HBox();
        agregarGrid = new GridPane();
        agregarGrid.setHgap(5);
        agregarGrid.setVgap(5);
        
        Label RegistroL;
        RegistroL = new Label("Ingresar Libro");
        RegistroL.setFont(Font.font("Ingresar Libro", FontWeight.BLACK, 20));
        agregarGrid.add(RegistroL,0,1);
        
        final Label idAutor;
        idAutor = new Label("ID libro(Modificar)");
        agregarGrid.add(idAutor,0,2);
        
        idAutor.setVisible(false);
       
        CampIdLibros = new TextField();
        agregarGrid.add(CampIdLibros,1,2);
        
        CampIdLibros.setPromptText("ID Libro");
        CampIdLibros.setVisible(false);
        
        Label nombreAutor12;
        nombreAutor12 = new Label("ID Autor");
        agregarGrid.add(nombreAutor12,0,3);
        
        CampIdAutorLibro = new TextField();
        agregarGrid.add(CampIdAutorLibro,1,3);
        
        CampIdAutorLibro.setPromptText("ID Autor");
        
        Label generoLibro12;
        generoLibro12 = new Label("ID Genero");
        agregarGrid.add(generoLibro12,0,4);
        
        CampIdGenero = new TextField();
        agregarGrid.add(CampIdGenero,1,4);
        
        CampIdGenero.setPromptText("ID Genero");
        
        Label nombreEdit12;
        nombreEdit12 = new Label("ID Editorial");
        agregarGrid.add(nombreEdit12, 0, 5);
        
        CampIdEditorial = new TextField();
        agregarGrid.add(CampIdEditorial,1,5);
        
        CampIdEditorial.setPromptText("ID Editorial");
        
        Label nombreLibro12;
        nombreLibro12 = new Label("Nombre Libro");
        agregarGrid.add(nombreLibro12, 0, 6);
        
        CampNombreLibro = new TextField();
        agregarGrid.add(CampNombreLibro,1,6);
        
        CampNombreLibro.setPromptText("Nombre");
      
        Label estadoLibroC;
        estadoLibroC = new Label("Estado Libro");
        agregarGrid.add(estadoLibroC,0,7);
        
        CampEstadoLibro = new TextField(); 
        agregarGrid.add(CampEstadoLibro, 1, 7);
        
        CampEstadoLibro.setPromptText("Estado Libro");
        
        final Button button = new Button("Agregar");
        agregarGrid.add(button,1,8);
        
        final Button button1 = new Button("Modificar");
        agregarGrid.add(button1, 1, 9);
        button1.setVisible(false);
        
        final Button salirLibr = new Button("Salir Modificar");
        agregarGrid.add(salirLibr,1,12);
        
        salirLibr.setVisible(false);
        
        button.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
               handlerLibro.getInstancia().agregar(Integer.parseInt(CampIdAutorLibro.getText()),Integer.parseInt(CampIdGenero.getText()),Integer.parseInt(CampIdEditorial.getText()),CampNombreLibro.getText(), CampEstadoLibro.getText());
               limpiarL();
            }
        });
        
         tab.setOnMouseClicked(new EventHandler<MouseEvent>(){
           
            @Override
            public void handle(MouseEvent t) {
               
                if (tab.getSelectionModel().getSelectedItem() != null) {

                     button.setVisible(false);
                     salirLibr.setVisible(true);
                     idAutor.setVisible(true);
                     CampIdLibros.setVisible(true);
                     button1.setVisible(true);
                  
                Libro libro = (Libro)tab.getSelectionModel().getSelectedItem();
                
                CampIdLibros.setText(Integer.toString(libro.getIdLibros()));
                CampIdAutorLibro.setText(libro.getNombreAutor());
                CampIdGenero.setText(libro.getNombreGenero());
                CampIdEditorial.setText(libro.getNombreEditorial());
                CampNombreLibro.setText(libro.getNombreLibro());
                CampEstadoLibro.setText(libro.getEstadoLibro());
                
                } else {
                    System.out.println("Lugar no valido");
                }
                    
            }    
            
        });
        
        
        salirLibr.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
             button.setVisible(true);
             salirLibr.setVisible(false);
             idAutor.setVisible(false);
             CampIdLibros.setVisible(false);
             button1.setVisible(false);
                
                CampIdLibros.setText(null);
                CampIdAutorLibro.setText(null);
                CampIdGenero.setText(null);
                CampIdEditorial.setText(null);
                CampNombreLibro.setText(null);
                CampEstadoLibro.setText(null);
      
            }
        });
        
        button1.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
               handlerLibro.getInstancia().modificarL(Integer.parseInt(CampIdLibros.getText()),Integer.parseInt(CampIdAutorLibro.getText()),Integer.parseInt(CampIdGenero.getText()),Integer.parseInt(CampIdEditorial.getText()),CampNombreLibro.getText(), CampEstadoLibro.getText());
              limpiarL();
            }
        });
        
        final ContextMenu contextMenu;
        contextMenu = new ContextMenu();
        
    contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Eliminar");
        }   
    });
    
    contextMenu.setOnShown(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent e) {
            System.out.println("Cambiar");
        }
    });

    MenuItem item1 = new MenuItem("Eliminar");
    item1.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            if (tab.getSelectionModel().getSelectedItem() != null) {
                handlerLibro.getInstancia().eliminar(tab.getSelectionModel().getSelectedItem().getIdLibros());
                limpiarL();
            }
        }
    });
    MenuItem item2 = new MenuItem("Preferences");
    item2.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e) {
            System.out.println("Preferences");
        }
    });
    contextMenu.getItems().addAll(item1, item2);
    
    tab.setContextMenu(contextMenu);
    
        hbox1.getChildren().add(agregarGrid);
    
        Gridpane.add(tab,0 ,2, 2, 1);
        Gridpane.setPadding(new Insets(20, 20, 20, 20));
        Gridpane.setHgap(75);
        Gridpane.setVgap(15);
        hbOx.getChildren().addAll(Gridpane,hbox1);    
        return hbOx;
    }
    
    public void limpiarL(){
        observableListLibro1.clear();
        handlerLibro.getInstancia().getListaLibro().clear();
        observableListLibro1 =  FXCollections.observableArrayList(handlerLibro.getInstancia().getListaLibro());
        
        tab.setItems(observableListLibro1);
    }
}
    
