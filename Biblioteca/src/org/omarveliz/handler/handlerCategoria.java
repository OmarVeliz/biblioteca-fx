package org.omarveliz.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.bean.Categoria;
import org.omarveliz.connection.Conexion;

public class handlerCategoria {
    private static handlerCategoria instancia;
    private Categoria categoria;
    private ArrayList<Categoria> listaCat = new ArrayList();
    
    
    public static handlerCategoria getInstancia() {
        if(instancia == null) {
            instancia = new handlerCategoria();
        }
        return instancia;
    }
    
    public Categoria getCategoria() {
        return categoria;
    }
    
    
     public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Categoria WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Categoria cat1 = new Categoria();
                    try {
                        cat1.setIdCategoria(resultset.getInt("idCategoria"));
                        cat1.setIdLibro(resultset.getInt("idLibro"));
                        cat1.setNombreCategoria(resultset.getString("nombreCategoria"));
                        cat1.setNombreGenero(resultset.getString("nombreGenero1"));
                        cat1.setAutoresCategoria(resultset.getString("autoresCategoria"));
                        this.categoria = cat1;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
     
     public ArrayList<Categoria> getListaCategoria(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Categoria;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                   Categoria cat1 = new Categoria();
                    try {
                        cat1.setIdCategoria(resultset.getInt("idCategoria"));
                        cat1.setIdLibro(resultset.getInt("idLibro"));
                        cat1.setNombreCategoria(resultset.getString("nombreCategoria"));
                        cat1.setNombreGenero(resultset.getString("nombreGenero1"));
                        cat1.setAutoresCategoria(resultset.getString("autoresCategoria"));
                        listaCat.add(cat1);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaCat;
    }  
 
     
    public void Agregar(int idLibro, String nombreCategoria, String nombreGenero1, String autoresCategoria) {
         Conexion.getInstancia().consultar("EXECUTE insertarCategoria "+idLibro+",'"+nombreCategoria+"','"+nombreGenero1+"','"+autoresCategoria+"';");
    } 
     
}
