package org.omarveliz.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.bean.Editorial;
import org.omarveliz.connection.Conexion;

public class handlerEditorial {
    private static handlerEditorial instancia;
    private Editorial editorial;
    private ArrayList<Editorial> listaEditorial = new ArrayList();
    private ArrayList<Editorial> listaEditBusc = new ArrayList();
   
    public handlerEditorial() {
    
    }
    
    public static handlerEditorial getInstancia() {
        if (instancia == null) {
            instancia = new handlerEditorial();
        }
        return instancia;
    }
    
    public Editorial getEditorial(){
        return editorial;
    }
    
    public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Editorial WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Editorial edit = new Editorial();
                    try {
                        edit.setIdEditorial(resultset.getInt("idEditorial"));
                        edit.setNombreEditorial(resultset.getString("nombreEditorial"));
                        edit.setDistribuidores(resultset.getString("distribuidores"));
                        this.editorial = edit;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
    
    public ArrayList<Editorial> getListaEditorial(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Editorial;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Editorial Edit = new Editorial();
                    try {
                        Edit.setIdEditorial(resultset.getInt("idEditorial"));
                        Edit.setNombreEditorial(resultset.getString("nombreEditorial"));
                        Edit.setDistribuidores(resultset.getString("distribuidores"));            
                        listaEditorial.add(Edit);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }   
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaEditorial;
    } 
    
    public void agregarEdit(String nombreEditorial, String distribuidores) {
        Conexion.getInstancia().consultar("EXECUTE agregarEdi '"+nombreEditorial+"','"+distribuidores+"';");     
    }
    
    public void modificarEdit(int idEditorial, String nombreEditorial, String distribuidores) {
         Conexion.getInstancia().consultar("EXECUTE modificarE "+idEditorial+",'"+nombreEditorial+"','"+distribuidores+"';");     
    }
    
    public void eliminarE(int idEditorial) {
         Conexion.getInstancia().consultar("EXECUTE eliminarEd "+idEditorial+";");   
    }
    
}
