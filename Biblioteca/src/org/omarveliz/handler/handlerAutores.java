package org.omarveliz.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.bean.Autores;
import org.omarveliz.connection.Conexion;

public class handlerAutores {
    
    private static handlerAutores instancia;
    private Autores autores;
    private ArrayList<Autores> listaAutores = new ArrayList();
    
    public static handlerAutores getInstancia() {
        if(instancia == null) {
            instancia = new handlerAutores();
        }
        return instancia;
    }
    
    public Autores getUsuario() {
        return autores;
    }
    
    public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Autores WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Autores aut = new Autores();
                    try {
                        aut.setIdAutor(resultset.getInt("idAutor"));
                        aut.setIdEditorial(resultset.getInt("idEditorial"));
                        aut.setIdGeneroAutor(resultset.getInt("idGeneroAutor"));
                        aut.setNombreAutor(resultset.getString("nombreAutor"));
                        aut.setColeccionAutor(resultset.getString("ColeccionAutor"));
                        this.autores = aut;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
    
    public ArrayList<Autores> getListaAutores(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Autores;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Autores autor = new Autores();
                    try {
                        autor.setIdAutor(resultset.getInt("idAutor"));
                        autor.setIdEditorial(resultset.getInt("idEditorial"));
                        autor.setIdGeneroAutor(resultset.getInt("idGeneroAutor"));
                        autor.setNombreAutor(resultset.getString("nombreAutor"));
                        autor.setColeccionAutor(resultset.getString("ColeccionAutor"));
                        listaAutores.add(autor);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaAutores;
    } 
    
    public void agregar(int idEditorial, int idGeneroAutor, String nombreAutor, String ColeccionAutor) {
        Conexion.getInstancia().consultar("EXECUTE agregarAut "+idEditorial+","+idGeneroAutor+",'"+nombreAutor+"','"+ColeccionAutor+"';");
    }
    
    public void modificar(int idAutor, int idEditorial, int idGenero, String nombreAutor, String coleccionAutor) {
        Conexion.getInstancia().consultar("EXECUTE modificarAut "+idAutor+","+idEditorial+","+idGenero+",'"+nombreAutor+"','"+coleccionAutor+"';");
    }
    
    public void eliminar(int idAutor) {
        Conexion.getInstancia().consultar("EXECUTE eliminarAutores "+idAutor+";");
    }
    
}
