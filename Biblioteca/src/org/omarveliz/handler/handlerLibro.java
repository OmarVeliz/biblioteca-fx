
package org.omarveliz.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.connection.Conexion;
import org.omarveliz.bean.Libro;
import org.omarveliz.connection.Conexion;

public class handlerLibro {
    
private static handlerLibro instancia;
    //private Object conexion;
    private Libro libro;
    private ArrayList<Libro> listaLibro = new ArrayList();
    
    private handlerLibro() {
    }
    
    public static handlerLibro getInstancia() {
        if(instancia == null) {
            instancia = new handlerLibro();
        }
        return instancia;
    }
    
    public Libro getLibro() {
        return libro;
    }
    
    public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Libros WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Libro libro = new Libro();
                    try {
                        libro.setIdLibros(resultset.getInt("idLibros"));
                        libro.setAutor(resultset.getString("idAutorLibro"));
                        libro.setGenero(resultset.getString("idGenero"));
                        libro.setEditorial(resultset.getString("idEditorial"));
                        libro.setNombreLibro(resultset.getString("nombreLibro"));  
                        libro.setEstadoLibro(resultset.getString("estadoLibro"));
                        this.libro = libro;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
    
    public ArrayList<Libro> getListaLibro(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM SeleccionLibros;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Libro libro = new Libro();
                    try {
                        libro.setIdLibros(resultset.getInt("idLibros"));
                        libro.setAutor(resultset.getString("nombreAutor"));
                        libro.setGenero(resultset.getString("nombreGenero"));
                        libro.setEditorial(resultset.getString("nombreEditorial"));
                        libro.setNombreLibro(resultset.getString("nombreLibro"));  
                        libro.setEstadoLibro(resultset.getString("estadoLibro"));
                        listaLibro.add(libro);
                    } catch (SQLException ex) {
                        Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(handlerLibro.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaLibro;
    }
    
    public void agregar(int idAutorLibro,int idGenero, int idEditorial, String nombreLibro, String estadoLibro) {
        Conexion.getInstancia().consultar("EXECUTE agregarL "+idAutorLibro+","+idGenero+","+idEditorial+",'"+nombreLibro+"','"+estadoLibro+"';");
    }
    
    public void modificarL(int idLibro,int AutorLibr, int idGenero, int idEditorial, String nombreLibro, String estadoLibro) {
        Conexion.getInstancia().consultar("EXECUTE ModificarLibro "+idLibro+","+AutorLibr+","+idGenero+","+idEditorial+",'"+nombreLibro+"','"+estadoLibro+"';");
    }
    
    public void eliminar(int idLibro) {
        Conexion.getInstancia().consultar("EXECUTE EliminarLibr "+idLibro+"';");
    }
 
}
