package org.omarveliz.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omarveliz.bean.Categoria;
import org.omarveliz.bean.Prestamo;
import org.omarveliz.connection.Conexion;

public class handlerPrestamos {
   private static handlerPrestamos instancia;
   private Prestamo prestamo;
   private ArrayList<Prestamo> listaPrest = new ArrayList();
    
   public static handlerPrestamos getInstancia() {
        if(instancia == null) {
            instancia = new handlerPrestamos();
        }
        return instancia;
    }
    
    public Prestamo getPrestamo() {
        return prestamo;
    }
    
    public boolean autentificar () {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Prestamos;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Prestamo prest = new Prestamo();
                    try {
                        prest.setIdPrestamo(resultset.getInt("idPrestamo"));
                        prest.setIdUsuario(resultset.getInt("idUsuario"));
                        prest.setIdLibro(resultset.getInt("idLibro"));
                        prest.setFechaPrestamo(resultset.getString("fechaPrestamo"));
                        prest.setFechaDevolucion(resultset.getString("fechaDevolucion"));
                        this.prestamo = prest;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }
    
     public ArrayList<Prestamo> getListaPrestamo(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Prestamos;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                   Prestamo prest = new Prestamo();
                    try {
                        prest.setIdPrestamo(resultset.getInt("idPrestamo"));
                        prest.setIdUsuario(resultset.getInt("idUsuario"));
                        prest.setIdLibro(resultset.getInt("idLibro"));
                        prest.setFechaPrestamo(resultset.getString("fechaPrestamo"));
                        prest.setFechaDevolucion(resultset.getString("fechaDevolucion"));
                        listaPrest.add(prest);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaPrest;
    }  
     
    public void agregar(int idUsuario, int idLibro, String fechaPrestamo, String fechaDevolucion) {
        Conexion.getInstancia().consultar("EXECUTE agregarPrestamo "+idUsuario+","+idLibro+",'"+fechaPrestamo+"','"+fechaDevolucion+"';");
    }
 
    public void modificar(int idPrestamo,int idUsuario, int idLibro, String fechaPrestamo, String fechaDevolucion) {
         Conexion.getInstancia().consultar("EXECUTE modificarPrest "+idPrestamo+","+idUsuario+","+idLibro+",'"+fechaPrestamo+"','"+fechaDevolucion+"';");
    }
    
    public void eliminarP(int idPrestamo) {
        Conexion.getInstancia().consultar("EXECUTE eliminarPrest "+idPrestamo+";");
    }
    
}
