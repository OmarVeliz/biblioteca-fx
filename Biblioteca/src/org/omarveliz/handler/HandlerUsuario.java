package org.omarveliz.handler;

import org.omarveliz.bean.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import org.omarveliz.connection.Conexion;

public class HandlerUsuario {
    private static HandlerUsuario instancia;
    //private Object conexion;
    private Usuario usuario;
    private ArrayList<Usuario> listaUsuario = new ArrayList();
    private ArrayList<Usuario> listaBuscar = new ArrayList();
  
    private HandlerUsuario() {
    }
    
    public static HandlerUsuario getInstancia() {
        if(instancia == null) {
            instancia = new HandlerUsuario();
        }
        return instancia;
    }
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    /*public boolean autentificar (String nombreUsuario, String contrasenia) {
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM Usuario WHERE nombreUsuario = '"+nombreUsuario+"' AND contrasenia = '"+contrasenia+"';");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Usuario user = new Usuario();
                    try {
                        user.setIdUsuario(resultset.getInt("IdUsuario"));
                        user.SetRol(resultset.getString("rol"));
                        user.setNombreUsuario(resultset.getString("nombreUsuario"));
                        user.setContrasenia(resultset.getString("contrasenia"));
                        this.usuario = user;
                        return true;
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return false;
    }*/
    
    public ArrayList<Usuario> getListaUsuario(){
        ResultSet resultset = Conexion.getInstancia().consultar("SELECT * FROM vistaRol;");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Usuario user = new Usuario();
                    try {
                        user.setIdUsuario(resultset.getInt("IdUsuario"));
                        user.SetRol(resultset.getString("rol"));
                        user.setNombreUsuario(resultset.getString("nombreUsuario"));
                        user.setContrasenia(resultset.getString("contrasenia"));
                        listaUsuario.add(user);
                    } catch (SQLException ex) {
                        Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaUsuario;
    } 
    
    public void agregar(String idRol,String nombreUsuario, String contrasenia) {
        Conexion.getInstancia().consultar("EXECUTE insertarUsuario "+idRol+",'"+nombreUsuario+"','"+contrasenia+"';");
    }
    
    public void eliminar() {
       Conexion.getInstancia().consultar("DELETE Usuario;");
    }
    
    public void eliminarLista(int idUsuario) {
        Conexion.getInstancia().consultar("EXECUTE eliminar '"+idUsuario+"';");
    }
    
     public ArrayList<Usuario> getListaBuscar(String criterio){
        ResultSet resultset = Conexion.getInstancia().consultar("EXECUTE BUSCAR "+criterio+";");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Usuario user = new Usuario();
                    try {
                        user.setIdUsuario(resultset.getInt("IdUsuario"));
                        user.SetRol(resultset.getString("rol"));
                        user.setNombreUsuario(resultset.getString("nombreUsuario"));
                        user.setContrasenia(resultset.getString("contrasenia"));
                        listaUsuario.add(user);
                    } catch (SQLException ex) {
                         Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }     
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return listaUsuario;
    } 
    
     public ArrayList<Usuario> buscar(String criterio) {
            ArrayList<Usuario> nuevaLista = new ArrayList<>(); 
            ResultSet resultset = Conexion.getInstancia().consultar("EXECUTE BUSCAR "+criterio+";");
        if(resultset != null) {
            try {
                while (resultset.next()) {
                    Usuario user = new Usuario();
                    try {
                        user.setIdUsuario(resultset.getInt("IdUsuario"));
                        user.SetRol(resultset.getString("rol"));
                        user.setNombreUsuario(resultset.getString("nombreUsuario"));
                        user.setContrasenia(resultset.getString("contrasenia"));
                        nuevaLista.add(user);
                    } catch (SQLException ex) {
                         Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }     
                }
            } catch (SQLException ex) {                
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return nuevaLista;   
     }       
     
     
     
     
     
     
     
     
    public void modificarUsuario(int idUsuario,String rol, String nombreUsuario, String contrasenia) {
        Conexion.getInstancia().consultar("EXECUTE modificarUsuario "+idUsuario+",'"+rol+"','"+nombreUsuario+"','"+contrasenia+"';");
    }
    
    public boolean autenticar(String nombreUsuario, String contrasenia) {
        ResultSet resultSet = Conexion.getInstancia().consultar("EXECUTE autenticar '"+nombreUsuario+"', '"+contrasenia+"';");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    Usuario user = new Usuario();
                    user.setIdUsuario(resultSet.getInt("idUsuario"));
                    user.SetRol(resultSet.getString("idRol"));
                    user.setNombreUsuario(resultSet.getString("nombreUsuario"));
                    user.setContrasenia(resultSet.getString("contrasenia"));
                    this.usuario = user;
                    return true;
                }
            } catch (SQLException ex) {
                Logger.getLogger(HandlerUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    
}   


