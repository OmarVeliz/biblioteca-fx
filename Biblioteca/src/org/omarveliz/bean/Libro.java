
package org.omarveliz.bean;

public class Libro {
    private int idLibros = 0;
    private String nombreAutor = null;
    private String nombreGenero = null;
    private String nombreEditorial = null;
    private String nombreLibro = null;
    private String estadoLibro = null;
    
    public void setIdLibros(int idLibros) {
        this.idLibros = idLibros;
    }
    
    public void setAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }
    
    public void setGenero(String nombreGenero) {
        this.nombreGenero = nombreGenero;
    } 
    
    public void setEditorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }
    
    public void setNombreLibro(String nombreLibro) {
        this.nombreLibro = nombreLibro;
    }
    
    public void setEstadoLibro(String estadoLibro) {
        this.estadoLibro = estadoLibro;
    }
    
    public int getIdLibros() {
        return idLibros;
    }
    
    public String getNombreAutor() {
        return nombreAutor;
    }
    
    public String getNombreGenero() {
        return nombreGenero;
    }

    public String getNombreEditorial() {
        return nombreEditorial;
    }
    
    public String getNombreLibro() {
        return nombreLibro;
    }
    
    public String getEstadoLibro() {
        return estadoLibro;
    }
}
