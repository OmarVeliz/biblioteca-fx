
package org.omarveliz.bean;

public class Prestamo {
    private int idPrestamo = 0;
    private int idUsuario = 0;
    private int idLibro = 0;
    private String fechaPrestamo = null;
    private String fechaDevolucion = null;
    
    public void setIdPrestamo(int idPrestamo) {
        this.idPrestamo = idPrestamo;
    }
    
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }
    
    public void setFechaPrestamo(String fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }
    
    public void setFechaDevolucion(String fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public int getIdPrestamo() {
        return idPrestamo;
    }
    
    public int getIdUsuario() {
        return idUsuario;
    }
    
    public int getIdLibro() {
        return idLibro;
    }
    
    public String getFechaPrestamo() {
        return fechaPrestamo;
    }
    
    public String getFechaDevolucion() {
        return fechaDevolucion;
    }
    
}


