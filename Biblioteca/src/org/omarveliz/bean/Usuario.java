package org.omarveliz.bean;

public class Usuario {
    private int idUsuario = 0;
    private String rol ;
    private String nombreUsuario = null;
    private String contrasenia = null;
    
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public void SetRol(String rol) {
        this.rol = rol;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    
    public int getIdUsuario(){
        return idUsuario;
    }
    
    public String getRol() {
        return rol;
    }
    
    public String getNombreUsuario() {
        return nombreUsuario;
    }
    
    public String getContrasenia() {
        return contrasenia;
    }
}

