/*INSERT INTO Usuario(idRol,nombreUsuario,contrasenia) VALUES
	(1,'admin','admin');

	select * from Usuario;*/

GO
CREATE PROCEDURE editarUsuario
@idRol int,
@nombreUsuario VARCHAR(255), 
@contrasenia VARCHAR(255)
AS
BEGIN 
UPDATE Usuario SET nombreUsuario = @nombreUsuario,
		contrasenia = @contrasenia WHERE idRol = @idRol;
END 
